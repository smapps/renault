//
//  APIClient.m
//  Renault
//
//  Created by Sameh Mabrouk on 5/15/14.
//
//

#import "APIClient.h"
#import "RenaultTargetCallback.h"

@implementation APIClient

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.cachedMetadataDictionary = [[NSMutableDictionary alloc] init];
        self.GDCachedMetadataDictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}
+ (instancetype)sharedClient {
    
    static APIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[[self class] alloc] init];
        
    });
    
    return _sharedClient;
}

- (void) callback: (id)result target:(RenaultTargetCallback *)target {
    NSLog(@"callback");

    NSLog(@"request result is %@",result);
    NSLog(@"response code %i",responseCode);
    if (result!= nil && (responseCode == 200)) {
        NSLog(@"return yes");
        target.callback(YES,result);
    } else {
        NSLog(@"return no");
        target.callback(NO,result);
    }
}


-(void)sendContactUsInfo:(NSString *)url
httpMethod:(NSString *)httpMethod withParams:(NSDictionary *)params
callback:(RenaultCallback)callback{

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    request.HTTPMethod = @"POST";
   
        NSLog(@"PUT Request");
        NSError *error = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        NSLog(@"data params %@",jsonData);
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"F$rzQxf12*Qs226" forHTTPHeaderField:@"Key"];
    [request setValue:@"1" forHTTPHeaderField:@"AppLang"];
    [request setValue:@"1.0.0" forHTTPHeaderField:@"AppVersion"];
    
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]]
       forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: jsonData];
        
        jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];
        NSMutableString* stringData= [[NSMutableString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Header params is %@",stringData);
    
    
    RenaultTargetCallback *target = [[RenaultTargetCallback alloc] initWithCallback:callback
                                                                                 resultCallback:@selector(callback:target:)
                                                                                     requestUrl: @"POST"
                                                                                       numTries: 2];
    
    [self makeAsyncRequestWithRequest:request
                               target:target];

}
@end
