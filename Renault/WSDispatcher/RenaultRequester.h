//
//  RenaultTargetCallback.h
//  Renault
//
//  Created by Sameh Mabrouk on 5/18/14.
//
//

#import <Foundation/Foundation.h>
#define TIMEOUT_INTERVAL 45

@class RenaultTargetCallback;
@interface RenaultRequester : NSObject
{

    int responseCode;
    NSURLConnection *conn;
}
@property (strong,nonatomic) NSMutableArray *requestHistory;
@property (strong, nonatomic)	NSMutableDictionary *asyncConnDict;

- (void) makeAsyncRequestWithRequest:(NSURLRequest *)urlRequest target:(RenaultTargetCallback *)target;
-(void)cancellAllRequests;

@end
