//
//  RenaultTargetCallback.m
//  Renault
//
//  Created by Sameh Mabrouk on 5/18/14.
//
//

#import "RenaultTargetCallback.h"

@implementation RenaultTargetCallback


- (id)initWithCallback:(callback_block)callback
        resultCallback:(SEL)aResultCallback
            requestUrl:(NSString *)aRequestUrl
              numTries:(int)numberTries {
    self = [super init];
    if (self) {
        self.callback = callback;
        self.resultCallback = aResultCallback;
        self.requestUrl = aRequestUrl;
        self.numTries = numberTries;
    }
	return self;
}



@end
