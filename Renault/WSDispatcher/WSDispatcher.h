//
//  WSDispatcher.h
//  Renault
//
//  Created by Manch on 1/29/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface WSDispatcher : AFHTTPRequestOperationManager

+ (WSDispatcher *)sharedWebServiceDispatcher;

- (void)GET:(NSString *)URL
 parameter:(id)parameter
    success:(void(^)(id responseObject))success
    failure:(void(^)(NSError *error))failure;

- (void)POST:(NSString *)URL
 parameter:(id)parameter
    success:(void(^)(id responseObject))success
    failure:(void(^)(NSError *error))failure;

@end