//
//  WSDispatcher+Setting.m
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher+Setting.h"
#import "Constant.h"

@implementation WSDispatcher (Setting)

- (void)getSettingsInformationWithSuccess:(void (^)(id))success failure:(void (^)(NSError *))failure {
    
    [self GET:kGetSettings parameter:nil success:^(id responseObject) {
        if (success)
            success(responseObject);
    } failure:^(NSError *error) {
        if (failure)
            failure(error);
    }];
}

@end
