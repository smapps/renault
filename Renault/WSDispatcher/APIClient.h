//
//  APIClient.h
//  Renault
//
//  Created by Sameh Mabrouk on 5/15/14.
//
//

#import <Foundation/Foundation.h>
#import "RenaultRequester.h"

typedef void(^RenaultCallback)(BOOL success, id result);


    
@interface APIClient : RenaultRequester<NSStreamDelegate>

@property(strong,nonatomic)NSMutableDictionary *cachedMetadataDictionary;
@property(strong,nonatomic)NSMutableDictionary *GDCachedMetadataDictionary;


-(void)sendContactUsInfo:(NSString *)url
           httpMethod:(NSString *)httpMethod withParams:(NSDictionary *)params
             callback:(RenaultCallback)callback;

+ (instancetype)sharedClient;

@end
