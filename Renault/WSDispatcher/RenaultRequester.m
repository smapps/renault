//
//  RenaultRequester.m
//  Renault
//
//  Created by Sameh Mabrouk on 5/18/14.
//
//

#import "RenaultRequester.h"
#import "RenaultTargetCallback.h"
#import "AppDelegate.h"

@implementation RenaultRequester
@synthesize asyncConnDict;
@synthesize requestHistory;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

- (id)init {
    self = [super init];
    if (self) {
        self.asyncConnDict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void)connectTarget:(RenaultTargetCallback *)target andConnection:(NSURLConnection *)connection {
    [asyncConnDict setValue:target forKey:[NSString stringWithFormat: @"%d", [connection hash]]];
}

- (void)disconnettargetWithConnection:(NSURLConnection *)connection {
    [asyncConnDict removeObjectForKey: [NSString stringWithFormat: @"%d", [connection hash]]];
}

- (RenaultTargetCallback *)targetForConnection:(NSURLConnection *)connection {
    return asyncConnDict[[NSString stringWithFormat: @"%d", [connection hash]]];
}


- (void) makeAsyncRequestWithRequest:(NSURLRequest *)urlRequest target:(RenaultTargetCallback *)target {
	conn = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self] ;
	
	if (conn) {
        NSLog(@"start Connection");
		// Create the NSMutableData that will hold the received data
		target.receivedData = [NSMutableData data];
        [self connectTarget:target andConnection:conn];
	} else {
        NSLog(@"Erro in Connection");
		NSMutableDictionary *dictionary  = [NSMutableDictionary dictionaryWithObject:@"async_conn_creation_failed"
                                                                              forKey:NSLocalizedDescriptionKey];
		NSError *error = [NSError errorWithDomain:@"Foursquare2"
                                             code:0
                                         userInfo:dictionary];
        if (target.resultCallback) {
            
            [self performSelector:target.resultCallback
                       withObject:error
                       withObject:target];
        }
	}
}

#pragma mark NSURLConnection


// fot untrusted stage
//from http://stackoverflow.com/questions/933331/how-to-use-nsurlconnection-to-connect-with-ssl-for-an-untrusted-cert
- (BOOL)connection:(NSURLConnection *)connection
canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    NSLog(@"canAuthenticateAgainstProtectionSpace");
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSLog(@"didReceiveAuthenticationChallenge");
    /*
     if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
     if ([trustedHosts containsObject:challenge.protectionSpace.host])
     [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
     
     [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
     */
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
        [challenge.sender useCredential:credential
             forAuthenticationChallenge:challenge];
    }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"didReceiveResponse");
	RenaultTargetCallback *target = [self targetForConnection:aConnection];
	NSMutableData *receivedData = [target receivedData];
    [receivedData setLength:0];
    
    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
    NSInteger statusCode = [HTTPResponse statusCode];
    NSLog(@"EZ status code is %i",statusCode);
    responseCode=statusCode;
    
}



- (void)connection:(NSURLConnection *)aConnection didReceiveData:(NSData *)data {
    NSLog(@"didReceiveData");
	RenaultTargetCallback *target = [self targetForConnection:aConnection];
	NSMutableData *receivedData = [target receivedData];
    [receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection {
    NSLog(@"connectionDidFinishLoading");
	RenaultTargetCallback *target = [self targetForConnection:aConnection];
	NSMutableData *receivedData = [target receivedData];
    NSDictionary *result;
    if (receivedData) {
        result = [NSJSONSerialization JSONObjectWithData:receivedData
                                                 options:0
                                                   error:nil];
//        NSLog(@"result JSON %@",result);
//        NSLog(@"returned Message is %@",[result objectForKey:@"message"]);
    }
    
	if (target.resultCallback) {
        NSLog(@"resultCallback SEL");
//        NSNumber *code = [result valueForKeyPath:@"meta.code"];
//        NSLog(@"status code1 %@",code);
        

        
        [self performSelector:target.resultCallback
                   withObject:result
                   withObject:target];
    }
	
	
    // release the connection, and the data object
    [self disconnettargetWithConnection:aConnection];
}

- (void)connection:(NSURLConnection *)aConnection didFailWithError:(NSError *)error {
	RenaultTargetCallback *target = [self targetForConnection:aConnection];
    if (target.resultCallback) {
        NSLog(@"didFailWithError");
        NSLog(@"Error is %@",error.description);
        
        responseCode = 0;
        
        [self performSelector:target.resultCallback
                   withObject:error
                   withObject:target];
    }
	[self disconnettargetWithConnection:aConnection];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	return nil;
}


-(NSURLRequest *)connection:(NSURLConnection *)connection
            willSendRequest:(NSURLRequest *)request
           redirectResponse:(NSURLResponse *)redirectResponse


{
    NSLog(@"cancelling URL Redirection");
    NSURLRequest *newRequest = request;
    NSLog(@"redirect URL %@",newRequest.URL);
    if (redirectResponse) {
        NSLog(@"Request Canceld...");
        newRequest = nil;
    }

    return newRequest;
    
}

-(void)cancellAllRequests{
    NSLog(@"cancel requests");
    [conn cancel];
}

@end
