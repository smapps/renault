//
//  WSDispatcher+DealersNetwork.m
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher+DealersNetwork.h"
#import "Constant.h"
#import "DealersNetworkModel.h"

@implementation WSDispatcher (DealersNetwork)

- (void)getDealersNetworkWithSuccess:(void (^)(id))success Failure:(void (^)(NSError *))failure {
    
    [self GET:kGetDealersNetwork parameter:nil success:^(id responseObject) {
        DealersNetworkModel *model = [DealersNetworkModel new];
        if (success){
        
//            [model cachData:responseObject[@"value"] fileName:@"carservices.json"];
            success([model mapNetworkLocations:responseObject[@"value"]]);
            /*
             
             [model cachData:responseObject[@"value"] fileName:@"dealersnetwork.json"];
             NSArray *cachedCarsDetails = [model loadCashedData:@"dealersnetwork.json"];
             
             NSArray *carsdetails = [model mapNetworkLocations:cachedCarsDetails];
             success(carsdetails);

             */
        }

    } failure:^(NSError *error) {
        if (failure)
            failure(error);
    }];
    
}

@end