//
//  WSDispatcher.m
//  Renault
//
//  Created by Manch on 1/29/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"
#import "Constant.h"

static WSDispatcher *_sharedWebServiceDispatcher = nil;

@implementation WSDispatcher

+ (WSDispatcher *)sharedWebServiceDispatcher {
    if (!_sharedWebServiceDispatcher) {
        _sharedWebServiceDispatcher = [[WSDispatcher alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
    }
    return _sharedWebServiceDispatcher;
}

- (void)GET:(NSString *)URL parameter:(id)parameter
    success:(void (^)(id))success
    failure:(void (^)(NSError *))failure {
    
    // Set Custom Headers
    NSString *versionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [self.requestSerializer setValue:versionNumber      forHTTPHeaderField:@"AppVersion"];
    [self.requestSerializer setValue:@"1"               forHTTPHeaderField:@"AppLang"];
    [self.requestSerializer setValue:@"F$rzQxf12*Qs226" forHTTPHeaderField:@"Key"];
    
    // Make a new operation to start GET request
    AFHTTPRequestOperation *operation = [self GET:URL parameters:parameter success:nil failure:nil];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.response.statusCode >= 200 && operation.response.statusCode < 300) {
            if (success)
                success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure)
            failure(error);
    }];
}

- (void)POST:(NSString *)URL parameter:(id)parameter
     success:(void (^)(id))success
     failure:(void (^)(NSError *))failure {
    
    // Set Custom Headers
    NSString *versionNumber = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [self.requestSerializer setValue:@"1.0.0"      forHTTPHeaderField:@"AppVersion"];
    [self.requestSerializer setValue:@"1"               forHTTPHeaderField:@"AppLang"];
    [self.requestSerializer setValue:@"F$rzQxf12*Qs226" forHTTPHeaderField:@"Key"];

    
    
    // Make a new operation to start GET request
    AFHTTPRequestOperation *operation = [self POST:URL parameters:parameter success:nil failure:nil];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (operation.response.statusCode >= 200 && operation.response.statusCode < 300) {
            if (success)
                success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure)
            failure(error);
    }];
}

@end