//
//  WSDispatcher+Services.m
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher+Services.h"
#import "Constant.h"
#import "ServiceModel.h"

@implementation WSDispatcher (Services)

- (void)getCarsServicesSuccess:(void (^)(id))success Failure:(void (^)(NSError *))failure {
    [self GET:kGetModelServices parameter:nil success:^(id responseObject) {
        ServiceModel *model = [[ServiceModel alloc] init];
        if (success) {
           
            [model cachData:responseObject[@"value"] fileName:@"carservices.json"];
            success([model mapServicesObjects:responseObject[@"value"]]);
        }
    } failure:^(NSError *error) {
        
    }];
}

@end
