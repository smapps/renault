//
//  WSDispatcher+ContactUs.h
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"

@interface WSDispatcher (ContactUs)

- (void)sendContactInformation:(NSString *)email
                        mobile:(NSString *)mobileNumber
                   contactType:(NSString *)type
                       inquiry:(NSString *)inquiry
                          name:(NSString *)name
                       success:(void(^)(id responseObject))success
                       failure:(void(^)(NSError *error))failure;
@end