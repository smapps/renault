//
//  WSDispatcher+Setting.h
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"

@interface WSDispatcher (Setting)

- (void)getSettingsInformationWithSuccess:(void(^)(id responseObject))success
                                  failure:(void(^)(NSError *error))failure;

@end