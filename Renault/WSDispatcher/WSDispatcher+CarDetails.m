//
//  WSDispatcher+CarDetails.m
//  Renault
//
//  Created by Manch on 2/5/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher+CarDetails.h"
#import "Constant.h"
#import "CarsDetails.h"
#import "OptionsModel.h"

@implementation WSDispatcher (CarDetails)

- (void)getAllCarsWithInformationsWithSuccess:(void (^)(id))success failure:(void (^)(NSError *))failure {
/*
    NSArray *cachedCarsDetails = [details loadCashedData:@"carlist.json"];
    if (cachedCarsDetails.count>0) {
        NSArray *carsdetails = [details mapCarsDetails:cachedCarsDetails];
        success(carsdetails);
        NSLog(@"success");
    }
    else{
 */
    NSLog(@"Model URL %@",kGetAllModels);
        [self GET:kGetAllModels parameter:nil success:^(id responseObject) {
            CarsDetails *details = [[CarsDetails alloc] init];
            NSArray *carsdetails = [details mapCarsDetails:responseObject[@"value"]];
            [details cachData:responseObject[@"value"] fileName:@"carlist.json"];
            
            if (success)
                success(carsdetails);
        } failure:^(NSError *error) {
            if (failure)
                failure(error);
        }];
    
}


- (void)getModelOptionsWithSuccess:(void (^)(id))success failure:(void (^)(NSError *))failure {
    
    [self GET:kGetModelOptions parameter:nil success:^(id responseObject) {
        OptionsModel *options = [[OptionsModel alloc] init];
        NSArray *optionsCategoryModels = [options mapCarModels:responseObject[@"value"]];
        [options cachData:responseObject[@"value"] fileName:@"modelOptions.json"];

        if (success)
            success(optionsCategoryModels);
    } failure:^(NSError *error) {
        if (failure)
            failure(error);
    }];
}

@end