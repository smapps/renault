//
//  WSDispatcher+CarDetails.h
//  Renault
//
//  Created by Manch on 2/5/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"

@interface WSDispatcher (CarDetails)

- (void)getAllCarsWithInformationsWithSuccess:(void(^)(id responseObject))success
                                      failure:(void(^)(NSError *error))failure;

- (void)getModelOptionsWithSuccess:(void(^)(id responseObject))success
                           failure:(void(^)(NSError *error))failure;

@end