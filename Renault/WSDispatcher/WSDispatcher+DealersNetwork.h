//
//  WSDispatcher+DealersNetwork.h
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"

@interface WSDispatcher (DealersNetwork)

- (void)getDealersNetworkWithSuccess:(void(^)(id responseObject))success Failure:(void(^)(NSError *error))failure;

@end