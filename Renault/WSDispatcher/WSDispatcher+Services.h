//
//  WSDispatcher+Services.h
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher.h"

@interface WSDispatcher (Services)

- (void)getCarsServicesSuccess:(void(^)(id responseObject))success Failure:(void(^)(NSError *error))failure;

@end