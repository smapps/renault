//
//  WSDispatcher+ContactUs.m
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "WSDispatcher+ContactUs.h"
#import "Constant.h"

@implementation WSDispatcher (ContactUs)

- (void)sendContactInformation:(NSString *)email
                        mobile:(NSString *)mobileNumber
                   contactType:(NSString *)type
                       inquiry:(NSString *)inquiry
                          name:(NSString *)name
                       success:(void(^)(id responseObject))success
                       failure:(void(^)(NSError *error))failure {
    
    NSDictionary *params = [NSDictionary dictionaryWithObjects:@[email, mobileNumber, type, inquiry, name]
                                                       forKeys:@[@"Email", @"MobileNo", @"ContactType", @"Inquiry", @"Name"]];
    
    [self POST:kContactUsURL parameter:params success:^(id responseObject) {
        if (success)
            success(responseObject);
    } failure:^(NSError *error) {
        if (failure)
            failure(error);
    }];
}

@end