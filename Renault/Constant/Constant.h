//
//  Constant.h
//  Renault
//
//  Created by Manch on 2/1/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#ifndef Renault_Constant_h
#define Renault_Constant_h


#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)

#define OrangeColor      [UIColor colorWithRed:252.0/255.0 green:180.0/255.0 blue:22.0/255.0 alpha:1.0]
#define DarkColor        [UIColor colorWithRed:0.0/255.0 green:18.0/255.0 blue:35.0/255.0 alpha:1.0]
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define AppLanguage  @"AppLanguage"
#define ImageBaseURL @"ImageBaseURL"
#define NewsLetterImageURL @"NewsLetterURL"

#define kBaseURL               @"http://renaultapp.azurewebsites.net/"

//#define kGetAllModels          @"api/Models?$expand=Overviews,ModelsColors,ModelsGallery,Editions,Editions/Options,Editions/Specifications,Editions/Specifications/Option,Editions/Specifications/Option/OptionsCategorie"
#define kGetAllModels          @"api/Models?$expand=Overviews,ModelsColors,ModelsGallery,Editions"


#define kContactUsURL          @"api/ContactRequests"
#define kGetSettings           @"api/AppSettings"
#define kGetDealersNetwork     @"api/DealersNetworks"
#define kGetModelServices      @"api/ModelsServices?$expand=Services/ServicesItems"
#define kGetModelOptions       @"api/ModelsEditions?$expand=Editions/OptionsCategories,Editions/OptionsCategories/Options"


#endif