//
//  KNMultiItemSelector.m
//  KNFBFriendSelectorDemo
//
//  Created by Kent Nguyen on 4/6/12.
//  Copyright (c) 2012 Kent Nguyen. All rights reserved.
//

#import "KNMultiItemSelector.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Constant.h"

#pragma mark - Private Interface

@interface KNMultiItemSelector (){

    NSMutableArray *servicesSelected;

}

@end

#pragma mark - Implementation

@implementation KNMultiItemSelector {
    NSString * placeholderText;
}

@synthesize tableView, useTableIndex, selectedItems, searchTextField, allowSearchControl, allowModeButtons;
@synthesize useRecentItems, maxNumberOfRecentItems, recentItemStorageKey;
@synthesize dismissOption;


-(id)initWithItems:(NSArray*)_items _itemsIDs:(NSArray*)_itemsIDs
  preselectedItems:(NSArray*)_preselectedItems
             title:(NSString*)_title
   placeholderText:(NSString*)_placeholder
          delegate:(id)delegateObject{

    self = [super init];
    if (self) {
        delegate = delegateObject;
        self.title = _title;
        self.maxNumberOfRecentItems = 5;
        self.useRecentItems = NO;
        self.recentItemStorageKey = @"recent_selected_items";
        self.allowModeButtons = YES;
        
        placeholderText = _placeholder;
        NSLog(@"init with items");
        // Initialize item arrays
        items = [_items mutableCopy];
        itemsIDs=[_itemsIDs mutableCopy];
        NSLog(@"items Count %lu",(unsigned long)itemsIDs.count);
        NSLog(@"preSelected %@",_preselectedItems);
        
        NSMutableArray *itemsArr = [[NSMutableArray alloc] init];
        for (int i=0; i<items.count; i++) {
            KNSelectorItem *item=[items objectAtIndex:i];
            [itemsArr addObject:item.displayValue];
        }
//        NSArray *arr =[[NSArray alloc] initWithObjects:@"Servicing", @"Show Rooms",nil];
//        NSLog(@"itemsArr %@",itemsArr);

        NSArray *savedSelectedServices=[[NSUserDefaults standardUserDefaults] objectForKey:@"Services"];
        NSLog(@"savedSelectedServices %@",savedSelectedServices);
        
        
        if (_preselectedItems) {
            selectedItems = _preselectedItems;
            NSLog(@"selectedItems %@",selectedItems);
            NSLog(@"items %@",items);
            for (KNSelectorItem * i in selectedItems) {
                for (int j=0; j<savedSelectedServices.count; j++) {
                
                    if ([i.displayValue isEqualToString:[savedSelectedServices objectAtIndex:j]]) {
                        NSLog(@"Pre selected...");
                        i.selected = YES;
                        break;
                    }
                }
                
            }
        } else {
            for (KNSelectorItem * i in self.selectedItems) {
                NSLog(@"selected");
                i.selected = NO;
            }
        }
        
        [self.tableView reloadData];
        
        // Recent selected items section
        recentItems = [NSMutableArray array];
        NSMutableArray * rArr =[[NSUserDefaults standardUserDefaults] objectForKey:self.recentItemStorageKey];
        
        // Preparing indices and Recent items
        indices = [NSMutableDictionary dictionary];
        for (KNSelectorItem * i in items) {
            NSString * letter = [i.displayValue substringToIndex:1];
            if (![indices objectForKey:letter]) {
                [indices setObject:[NSMutableArray array] forKey:letter];
            }
            if ([rArr containsObject:i.selectValue]) {
                [recentItems addObject:i];
            }
            NSMutableArray * a = [indices objectForKey:letter];
            [a addObject:i];
        }
    }
    return self;

}


-(void)loadView {
    self.view = [[UIView alloc] initWithFrame:CGRectZero];
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Initialize tableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *image;
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    if (screenBounds.size.height == 568) {
        
        // code for 4-inch screen
        image= [UIImage imageNamed:@"iphone-5categoryBG.png"];
    }
    else
        image= [UIImage imageNamed:@"iphone-4categoryBG.png"];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [imgView setImage:image];
    
    [self.tableView setBackgroundView:imgView];
    
    [self.view addSubview:self.tableView];
    
    // Initialize search text field
    textFieldWrapper = [[UIView alloc] initWithFrame:CGRectZero];
    textFieldWrapper.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    textFieldWrapper.autoresizesSubviews = YES;
    textFieldWrapper.backgroundColor = [UIColor whiteColor];
    textFieldWrapper.layer.shadowColor = [[UIColor blackColor] CGColor];
    textFieldWrapper.layer.shadowOffset = CGSizeMake(0,1);
    textFieldWrapper.layer.shadowRadius = 5.0f;
    textFieldWrapper.layer.shadowOpacity = 0.2;
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectZero];
    self.searchTextField.backgroundColor = [UIColor whiteColor];
    self.searchTextField.clipsToBounds = NO;
    self.searchTextField.keyboardType = UIKeyboardTypeASCIICapable;
    self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.searchTextField.returnKeyType = UIReturnKeyDone;
    self.searchTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.searchTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.searchTextField.delegate = self;
    self.searchTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KNZoomIcon"]];
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    self.searchTextField.placeholder = placeholderText ? placeholderText : NSLocalizedString(@"Search by keywords", nil);
    self.searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [self.view addSubview:textFieldWrapper];
    [textFieldWrapper addSubview:self.searchTextField];
    
    // Image indicator
    modeIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KNSelectorTip"]];
    modeIndicatorImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    modeIndicatorImageView.contentMode = UIViewContentModeCenter;
    [self.view addSubview:modeIndicatorImageView];
    
    // Two mode buttons
    normalModeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedModeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [normalModeButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [normalModeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [normalModeButton setBackgroundImage:[UIImage imageNamed:@"cancel_btn.png"] forState:UIControlStateNormal];
    [selectedModeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [normalModeButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [selectedModeButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    [selectedModeButton setBackgroundImage:[UIImage imageNamed:@"done_btn.png"] forState:UIControlStateNormal];
    [normalModeButton addTarget:self action:@selector(modeButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    [selectedModeButton addTarget:self action:@selector(modeButtonDidTouch:) forControlEvents:UIControlEventTouchUpInside];
    normalModeButton.titleLabel.font = selectedModeButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
    normalModeButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    selectedModeButton.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [normalModeButton setSelected:YES];
    [self.view addSubview:normalModeButton];
    [self.view addSubview:selectedModeButton];
    [self updateSelectedCount];
    
    
    //Nav bar Style
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};

    
    // Nav bar button
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [backButton setImage:[UIImage imageNamed:@"Back-icon_24.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didCancel) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* leftItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [doneButton setTitle:@"Done" forState:UIControlStateNormal];
  //  [doneButton setImage:[UIImage imageNamed:@"Back-icon_24.png"] forState:UIControlStateNormal];
    [doneButton addTarget:self action:@selector(didFinish) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    
   // self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(didFinish)];
 //   self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(didCancel)];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Layout UI elements
    CGRect f = self.view.frame;
    textFieldWrapper.frame = CGRectMake(0, 0, f.size.width, 44);
    self.searchTextField.frame = CGRectMake(6,6, f.size.width-12, 32);
    
    // Show or hide search control
    if ((textFieldWrapper.hidden = !self.allowSearchControl)) {
        self.tableView.frame = CGRectMake(0, 0, f.size.width, f.size.height - 40);
    } else {
        self.tableView.frame = CGRectMake(0, textFieldWrapper.frame.size.height, f.size.width, f.size.height - textFieldWrapper.frame.size.height - 40);
    }
    
    normalModeButton.frame = CGRectMake(6, f.size.height-50, 147, 44);
    if (IS_IPAD) {
        selectedModeButton.frame = CGRectMake(167, f.size.height-50, 147, 44);
    }
    else
        selectedModeButton.frame = CGRectMake(167, f.size.height-50, 130, 44);
    
    modeIndicatorImageView.center = CGPointMake(normalModeButton.center.x, f.size.height-44+modeIndicatorImageView.frame.size.height/2);
    
    [self showHideModeButtons];
}

-(void)showHideModeButtons {
    normalModeButton.hidden = selectedModeButton.hidden = modeIndicatorImageView.hidden = !self.allowModeButtons;
    
    CGRect tableFrame = self.tableView.frame;
    
    if (self.allowModeButtons) {
        tableFrame.size.height = CGRectGetMinY(modeIndicatorImageView.frame) - CGRectGetMinY(tableFrame);
    } else {
        tableFrame.size.height = CGRectGetHeight(self.view.bounds) - CGRectGetMinY(tableFrame);
    }
    
    self.tableView.frame = tableFrame;
}

-(void)setAllowModeButtons:(BOOL)allow {
    allowModeButtons = allow;
    [self showHideModeButtons];
}

-(void)updateSelectedCount {
    NSUInteger count = self.selectedItems.count;
    if (count == 0) {
        [selectedModeButton setTitle:NSLocalizedString(@"Done", @"0 is the initial count; nothing selected.") forState:UIControlStateNormal];
    } else {
        [selectedModeButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"Done", @"%d is the count of selected items"), self.selectedItems.count] forState:UIControlStateNormal];
    }
}

#pragma mark - UITableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (selectorMode == KNSelectorModeNormal) {
        int noSec = useTableIndex ? [[self sortedIndices] count] : 1;
        return self.useRecentItems && recentItems.count ? noSec+1 : noSec;
    } else {
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (selectorMode == KNSelectorModeSearch) {
        return filteredItems.count;
    } else if (selectorMode == KNSelectorModeNormal) {
        if (useRecentItems && section==0 && recentItems.count) {
            return recentItems.count;
        } else if (useTableIndex) {
            if (useRecentItems && recentItems.count) section -= 1;
            NSMutableArray * rows = [indices objectForKey:[[self sortedIndices] objectAtIndex:section]];
            return rows.count;
        } else {
            return items.count;
        }
    } else {
        return self.selectedItems.count;
    }
}

-(UITableViewCell*)tableView:(UITableView *)_tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"KNSelectorItemCell";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor blackColor];
    
    UILabel *itemLabel = [[UILabel alloc] initWithFrame:CGRectMake(46, 13, 188, 17)];
    itemLabel.textColor= [UIColor lightGrayColor];
    
    
    // Which item?
    KNSelectorItem * item = [self itemAtIndexPath:indexPath];
    
    // Change the cell appearance
    itemLabel.text = item.displayValue;
    [cell addSubview:itemLabel];
    
    if (item.imageUrl) {
        [cell.imageView setImageWithURL:[NSURL URLWithString:item.imageUrl] placeholderImage:[UIImage imageNamed:@"KNDefaultImage"]];
    }
    NSLog(@"cellForRowAtIndexPath");
    NSLog(@"item selected %d",item.selected);
    
    UIImageView *indicatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(270, 3, 15, 15)];
    [indicatorImgView setImage:[UIImage imageNamed:@"check_icon.png"]];
    cell.accessoryView = item.selected ? indicatorImgView : nil;
    
    UIImageView *itemImageView=[[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 30, 30)];
    NSString *partsImg;
    NSString *serviceImg;
    NSString *roomImg;
    if (IS_IPAD) {
        partsImg=@"parts_icon~ipad";
        serviceImg=@"servicing_icon~ipad";
        roomImg=@"show_room_icon~ipad";
    }
    else{
        partsImg=@"parts_icon";
        serviceImg=@"servicing_icon";
        roomImg=@"show_room_icon";
    }

    
    
    if (indexPath.row==1) {
        [itemImageView setImage:[UIImage imageNamed:partsImg]];
        [cell addSubview:itemImageView];
    }
    else if (indexPath.row==2) {
        [itemImageView setImage:[UIImage imageNamed:serviceImg]];
        [cell addSubview:itemImageView];
    }
    else if (indexPath.row==3) {
        [itemImageView setImage:[UIImage imageNamed:roomImg]];
        [cell addSubview:itemImageView];
    }
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)_tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    // Which item?
    KNSelectorItem * item = [self itemAtIndexPath:indexPath];
    item.selected = !item.selected;
    
    // Recount selected items
    [self updateSelectedCount];
    
    // Update UI
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
 //   [_tableView cellForRowAtIndexPath:indexPath].accessoryType = item.selected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    UIImageView *indicatorImgView = [[UIImageView alloc] initWithFrame:CGRectMake(270, 3, 15, 15)];
    [indicatorImgView setImage:[UIImage imageNamed:@"check_icon.png"]];
     [_tableView cellForRowAtIndexPath:indexPath].accessoryView = item.selected ? indicatorImgView : nil;
    
    if ([self.searchTextField isFirstResponder]) {
        self.searchTextField.tag = 1;
        [self.searchTextField resignFirstResponder];
    }
    if (indexPath.row==0) {
     
        //unselect the rest of list.

        for (KNSelectorItem * i in self.selectedItems) {
            if (![i.displayValue isEqualToString:@"All Services"]) {
            
                i.selected = NO;
                servicesSelected=[[NSMutableArray alloc] init];
            }
            
        }
        [self.tableView reloadData];
        
    }
    else{
        //unselect the first item.
        for (KNSelectorItem * i in self.selectedItems) {
            if ([i.displayValue isEqualToString:@"All Services"]) {
                
                i.selected = NO;
                servicesSelected=[[NSMutableArray alloc] init];
            }
            
        }
        
        [self.tableView reloadData];
    }
    // Delegate callback
    if (item.selected) {
        if ([delegate respondsToSelector:@selector(selectorDidSelectItem:)]) [delegate selectorDidSelectItem:item];
    } else {
        if ([delegate respondsToSelector:@selector(selectorDidDeselectItem:)]) [delegate selectorDidDeselectItem:item];
        if (selectorMode==KNSelectorModeSelected) {
            [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    
    if (!servicesSelected) {
        NSArray *savedSelectedServices=[[NSUserDefaults standardUserDefaults] objectForKey:@"Services"];
        NSLog(@"savedSelectedServices %@",savedSelectedServices);
        if (savedSelectedServices) {
            servicesSelected=[[NSMutableArray alloc] initWithArray:savedSelectedServices];
        }
        else
            servicesSelected=[[NSMutableArray alloc] init];
    }
    if (item.selected) {
        [servicesSelected addObject:item.displayValue];
    }
    else{
    
        if ([servicesSelected containsObject:item.displayValue]) {
            [servicesSelected removeObject:item.displayValue];
        }
    }
    NSLog(@"servicesSelected %@",servicesSelected);
}
-(void)unSelectItems{

    
}
#pragma mark - UITextfield Delegate & Filtering

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * searchString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (searchString.length > 0) {
        selectorMode = KNSelectorModeSearch;
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"displayValue LIKE[cd] %@ OR displayValue LIKE[cd] %@",
                             [searchString stringByAppendingString:@"*"],
                             [NSString stringWithFormat:@"* %@*",searchString]];
        filteredItems = [items filteredArrayUsingPredicate:pred];
    } else {
        selectorMode = KNSelectorModeNormal;
    }
    [self.tableView reloadData];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    selectorMode = KNSelectorModeNormal;
    [self.tableView reloadData];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.searchTextField.tag == 1) {
        self.searchTextField.tag = 0;
        self.searchTextField.text = @"";
    }
    return YES;
}

#pragma mark - Custom getters/setters

-(NSArray*)selectedItems {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"selected = YES"];
    return [items filteredArrayUsingPredicate:pred];
}

-(NSArray*)sortedIndices {
    return [indices.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

#pragma mark - Helpers

-(KNSelectorItem*)itemAtIndexPath:(NSIndexPath*)indexPath {
    // Determine the correct item at different settings
    int r = indexPath.row;
    int s = indexPath.section;
    if (selectorMode == KNSelectorModeSearch) {
        return [filteredItems objectAtIndex:r];
    }
    
    if (selectorMode == KNSelectorModeNormal) {
        if (self.useRecentItems && recentItems.count && s==0) {
            return [recentItems objectAtIndex:r];
        }
        if (useTableIndex) {
            if(self.useRecentItems && recentItems.count) s-=1;
            NSMutableArray * rows = [indices objectForKey:[[self sortedIndices] objectAtIndex:s]];
            return [rows objectAtIndex:r];
        }
        return [items objectAtIndex:r];
    }
    
    if (selectorMode == KNSelectorModeSelected) {
        return [self.selectedItems objectAtIndex:r];
    }
    
    return [items objectAtIndex:r];
}

#pragma mark - Cancel or Done button event

-(void)didCancel {
    NSLog(@"didCancel");
    /*
     [delegate selectorDidCancelSelection];
     
     // Clear all selections
     for (KNSelectorItem * i in self.selectedItems) {
     i.selected = NO;
     }
     // Delegate callback
     if ([delegate respondsToSelector:@selector(selectorDidCancelSelection)]) {
     NSLog(@"respond");
     [delegate selectorDidCancelSelection];
     }
     */
    for (KNSelectorItem * i in self.selectedItems) {
        i.selected = NO;
    }
    
    //dismiss current View
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(void)didFinish {
    
    NSString * text = @"Selected: ";
    for (KNSelectorItem * i in self.selectedItems){
        text = [text stringByAppendingFormat:@"%@,", i.selectValue];
    }
    
    //save Preferences
    NSLog(@"text is %@",text);
    NSMutableArray *preferences = [NSMutableArray new];

    for (int i = 0; i < [itemsIDs count]; i++) {
       
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        KNSelectorItem * item = [self itemAtIndexPath:indexPath];
        NSNumber *wrappedBool;
        if (item.selected) {
            wrappedBool = [NSNumber numberWithBool:YES];
            NSLog(@"selected item is %@",item.selectValue);
        }
        else{
            wrappedBool = [NSNumber numberWithBool:NO];
        }
        NSDictionary *dict = [NSDictionary
                              dictionaryWithObjects:[NSArray arrayWithObjects:[itemsIDs objectAtIndex:i] ,wrappedBool, nil]
                              forKeys:[NSArray arrayWithObjects:@"tag_id",@"status", nil]];
        
        [preferences addObject:dict];
    }
    NSLog(@"preference array %@",preferences);

    
    
    /*
    
    // Store recent items FIFO
    if (self.useRecentItems && self.maxNumberOfRecentItems < items.count) {
        NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
        NSMutableArray * array = [(NSMutableArray*)[defaults objectForKey:self.recentItemStorageKey] mutableCopy];
        if (!array) array = [NSMutableArray array];
        for (KNSelectorItem * i in self.selectedItems) {
            [array insertObject:i.selectValue atIndex:0];
        }
        while (array.count > self.maxNumberOfRecentItems) {
            [array removeLastObject];
        }
        [defaults setObject:array forKey:self.recentItemStorageKey];
        [defaults synchronize];
    }
    
    //dismiss current View
    //    [self dismissViewControllerAnimated:YES completion:nil];
   */
    
}

#pragma mark - Handle mode switching UI

-(void)modeButtonDidTouch:(id)sender {
    UIButton * s = (UIButton*)sender;
    if (s == normalModeButton) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissPopViewController" object:nil];
    }
    else{
        NSLog(@"servicesSelected %@",servicesSelected);
        //save selectedservices.
        [[NSUserDefaults standardUserDefaults] setObject:servicesSelected forKey:@"Services"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FilterSearchNotification" object:servicesSelected];
    }
    

    
    /*
    UIButton * s = (UIButton*)sender;
    if (s.selected) return;
    
    if (s == normalModeButton) {
        selectorMode = self.searchTextField.text.length > 0 ? KNSelectorModeSearch : KNSelectorModeNormal;
        normalModeButton.selected = YES;
        selectedModeButton.selected = NO;
        [self.tableView reloadData];
        [UIView animateWithDuration:0.3 animations:^{
            if (!textFieldWrapper.hidden) {
                CGRect f = self.tableView.frame;
                f.origin.y = textFieldWrapper.frame.size.height;
                f.size.height -= f.origin.y;
                self.tableView.frame = f;
                textFieldWrapper.alpha = 1;
            }
            modeIndicatorImageView.center = CGPointMake(normalModeButton.center.x, modeIndicatorImageView.center.y);
        }];
    } else {
        selectorMode = KNSelectorModeSelected;
        normalModeButton.selected = NO;
        selectedModeButton.selected = YES;
        [self.tableView reloadData];
        [UIView animateWithDuration:0.3 animations:^{
            if (!textFieldWrapper.hidden) {
                CGRect f = self.tableView.frame;
                f.origin.y = 0;
                f.size.height += textFieldWrapper.frame.size.height;
                self.tableView.frame = f;
                textFieldWrapper.alpha = 0;
            }
            modeIndicatorImageView.center = CGPointMake(selectedModeButton.center.x, modeIndicatorImageView.center.y);
        }];
    }
     */
    
}

#pragma mark - Table indices

- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section {
    if (selectorMode == KNSelectorModeNormal) {
        if (self.useRecentItems && recentItems.count) {
            if (section==0) return NSLocalizedString(@"Recent", nil);
            if (!useTableIndex) return @" ";
        }
        if (useTableIndex) {
            if(self.useRecentItems && recentItems.count) section-=1;
            return [[self sortedIndices] objectAtIndex:section];
        }
    }
    return nil;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if (selectorMode == KNSelectorModeNormal && useTableIndex) {
        if (self.useRecentItems && recentItems.count) {
            NSMutableArray * iArr = [[self sortedIndices] mutableCopy];
            [iArr insertObject:@"★" atIndex:0];
            return iArr;
        } else {
            return [self sortedIndices];
        }
    }
    return nil;
    return selectorMode == KNSelectorModeNormal && useTableIndex ? [self sortedIndices] : nil;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return index;
}

#pragma mark - Other memory stuff

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload {
    self.tableView = nil;
    self.searchTextField = nil;
    textFieldWrapper = nil;
    modeIndicatorImageView = nil;
    normalModeButton = nil;
    selectedModeButton = nil;
    
    [super viewDidUnload];
}

@end
