//
//  ImagePreviewViewController.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePreviewViewController : UIViewController

@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSArray  *imagesURL;
@property (assign) NSInteger selectedImageIndex;

- (IBAction)dismissPreview:(id)sender;
- (IBAction)nextImageTapped:(UIButton *)sender;
- (IBAction)previousImageTapped:(UIButton *)sender;

@property (retain, nonatomic) IBOutlet UIImageView *singleImagePreview;
@property (weak, nonatomic) IBOutlet UIButton *previousImage;
@property (weak, nonatomic) IBOutlet UIButton *nextImage;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollImage;


@end