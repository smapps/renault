//
//  ImageScrollViewController.m
//  ImageScroll
//
//  Created by Evgenii Neumerzhitckii on 19/05/13.
//  Copyright (c) 2013 Evgenii Neumerzhitckii. All rights reserved.
//

#import "ImageScrollViewController.h"
#import <UIImageView+AFNetworking.h>
#import "Constant.h"
#import "Utilities.h"
#import "LangaugeViewController.h"

@interface ImageScrollViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeft;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;
@property (weak, nonatomic) IBOutlet UIButton *changeImageButton;
@property (nonatomic) CGFloat lastZoomScale;

@end

@implementation ImageScrollViewController

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"viewWillAppear..Image");
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"Newsletter" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"اخبار" inViewController:self];
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    NSString *imageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:ImageBaseURL];
    NSString *newsLetterImageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:NewsLetterImageURL];

    /*

     self.scrollView.delegate = self;
     [self updateZoom];
     //    [self.scrollView setZoomScale:self.scrollView.bounds.size.width/self.imageView.bounds.size.width animated:NO];

     [self.scrollView setZoomScale:10 animated:NO];


     [self.imageView setImageWithURL:[NSURL URLWithString:[imageBaseURL stringByAppendingString:newsLetterImageBaseURL]]];

     //  self.imageView.image = [UIImage imageNamed: @"newsletter.png"];
     */

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[imageBaseURL stringByAppendingString:newsLetterImageBaseURL]]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];

    [self.imageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {

        self.imageView.image = image;

        self.scrollView.delegate = self;
        [self updateZoom];
        [self.scrollView setZoomScale:self.scrollView.bounds.size.width/self.imageView.bounds.size.width animated:NO];


    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {

    }];

}

// Update zoom scale and constraints
// It will also animate because willAnimateRotationToInterfaceOrientation
// is called from within an animation block
//
// DEPRECATION NOTICE: This method is said to be deprecated in iOS 8.0. But it still works.
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:interfaceOrientation duration:duration];

    [self updateZoom];
}

- (void) scrollViewDidZoom:(UIScrollView *)scrollView {
    [self updateConstraints];
}

- (void) updateConstraints {
    float imageWidth = self.imageView.image.size.width;
    float imageHeight = self.imageView.image.size.height;

    float viewWidth = self.view.bounds.size.width;
    float viewHeight = self.view.bounds.size.height;

    // center image if it is smaller than screen
    float hPadding = (viewWidth - self.scrollView.zoomScale * imageWidth) / 2;
    if (hPadding < 0) hPadding = 0;

    float vPadding = (viewHeight - self.scrollView.zoomScale * imageHeight) / 2;
    if (vPadding < 0) vPadding = 0;

    self.constraintLeft.constant = hPadding;
    self.constraintRight.constant = hPadding;

    self.constraintTop.constant = vPadding;
    self.constraintBottom.constant = vPadding;

    // Makes zoom out animation smooth and starting from the right point not from (0, 0)
    [self.view layoutIfNeeded];
}

// Zoom to show as much image as possible unless image is smaller than screen
- (void) updateZoom {
    float minZoom = MIN(self.view.bounds.size.width / self.imageView.image.size.width,
                        self.view.bounds.size.height / self.imageView.image.size.height);

    if (minZoom > 1) minZoom = 1;

    self.scrollView.minimumZoomScale = minZoom;
    //    self.scrollView.maximumZoomScale = 7.0;



    // Force scrollViewDidZoom fire if zoom did not change
    if (minZoom == self.lastZoomScale) minZoom += 0.000001;

    self.lastZoomScale = self.scrollView.zoomScale = minZoom;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.imageView;
}

- (IBAction)onImageChangeTouched:(id)sender {
    self.changeImageButton.selected = !self.changeImageButton.isSelected;
    [self.changeImageButton invalidateIntrinsicContentSize];

    NSString *fileName = @"wallabi.jpg";
    if (self.changeImageButton.selected ) fileName = @"wallabi_small.jpg";
    
    self.imageView.image = [UIImage imageNamed: fileName];
    [self updateZoom];
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }


    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {


        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }

    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"Newsletter" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"اخبار" inViewController:self];

}
@end
