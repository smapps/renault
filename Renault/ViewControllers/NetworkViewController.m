//
//  NetworkViewController.m
//  Renault
//
//  Created by Manch on 2/2/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "NetworkViewController.h"
#import "FilterViewController.h"
#import "LangaugeViewController.h"
#import "LocationAnnotation.h"
#import "Constant.h"
#import "Utilities.h"

#import "KNMultiItemSelector.h"
#import "CallOutAnnotationView.h"
#import "NetworkCell.h"
#import "WYPopoverController.h"
#import "CustomCalloutViewController.h"

#define MAXIMUM_ZOOM 20
#define METERS_PER_MILE 100

@interface NetworkViewController ()
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation NetworkViewController {
    NSArray *networkDealers;
    MKCoordinateRegion savedRegion;
    WYPopoverController *popover;
}

#pragma mark - Page Life Cycle -

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.splitViewController setDelegate:self];
    
    self.navigationItem.rightBarButtonItems = @[self.langaugeItem, self.filterItem];
    [self.navigationController setToolbarHidden:YES];
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    if (IS_IPAD) {
        UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"List"]
                                                       landscapeImagePhone:nil
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self.splitViewController.displayModeButtonItem.target
                                                                    action:self.splitViewController.displayModeButtonItem.action];
        [backItem setTintColor:DarkColor];
        self.navigationController.topViewController.navigationItem.leftBarButtonItem = backItem;
    }
    else
        self.navigationItem.hidesBackButton=YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadLocations:)
                                                 name:@"LoadLocationsOnMap"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setLocationForPlot:)
                                                 name:@"FoucsOnPlot"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(drawNewFilterResults:)
                                                 name:@"DrawFilterResultsNotification"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        self.title = @"Locations";
    else
        self.title = @"اماكن";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Map -

- (void)plotNetworkPositions {
    [self.mapView removeAnnotations:self.mapView.annotations];
    for (int i=0; i<networkDealers.count; i++) {
        
        DealersNetworkModel *loc = [networkDealers objectAtIndex:i];
//    for (DealersNetworkModel *loc in networkDealers) {
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = loc.latitude.doubleValue;
        coordinate.longitude = loc.longitude.doubleValue;
        LocationAnnotation *annotation = [[LocationAnnotation alloc] initWithID:loc.ID Name:[loc getName]
                                                                          address:[loc getAddress]
                                                                       coordinate:coordinate];
        annotation.locationNum=i;
        [self.mapView addAnnotation:annotation];
    }
    savedRegion = self.mapView.region;
}

- (void)loadLocations:(NSNotification *)locations {
    NSLog(@"loadLocations");
    networkDealers = [locations object];
    [self plotNetworkPositions];
}

- (void)loadSelectedLocation:(NSNotification *)notification {
    DealersNetworkModel *object = [notification object];
    [self setSelectedLocation:object];
}

- (void)setLocationForPlot:(NSNotification *)notification {
    DealersNetworkModel *object = [notification object];
    CLLocationCoordinate2D networkLocation = CLLocationCoordinate2DMake([object.latitude doubleValue],
                                                                        [object.longitude doubleValue]);
    MKCoordinateRegion viewregion = MKCoordinateRegionMakeWithDistance(networkLocation, 0.5 * METERS_PER_MILE,
                                                                       0.5 * METERS_PER_MILE);
    [self.mapView setRegion:viewregion animated:YES];
}

- (void)drawNewFilterResults:(NSNotification *)notification {
    networkDealers = [notification object];
    [self.popController dismissPopoverAnimated:YES];
//    [self.splitViewController setPreferredDisplayMode:UISplitViewControllerDisplayModePrimaryOverlay];
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (DealersNetworkModel *loc in networkDealers) {
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = loc.latitude.doubleValue;
        coordinate.longitude = loc.longitude.doubleValue;
        LocationAnnotation *annotation = [[LocationAnnotation alloc] initWithID:loc.ID Name:[loc getName]
                                                                        address:[loc getAddress]
                                                                     coordinate:coordinate];
        [self.mapView addAnnotation:annotation];
    }
    
    if ([notification object]) {
//        [self.mapView setRegion:savedRegion animated:YES];
    }
    
    //Hide popover.
    if (popover.isPopoverVisible) {
        [popover dismissPopoverAnimated:YES completion:^{
        }];
    }
    
}

- (MKMapRect)MKMapRectForCoordinateRegion:(MKCoordinateRegion)region
{
    MKMapPoint a = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                        region.center.latitude + region.span.latitudeDelta / 2,
                                        region.center.longitude - region.span.longitudeDelta / 2));
    MKMapPoint b = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                        region.center.latitude - region.span.latitudeDelta / 2,
                                        region.center.longitude + region.span.longitudeDelta / 2));
    return MKMapRectMake(MIN(a.x,b.x), MIN(a.y,b.y), ABS(a.x-b.x), ABS(a.y-b.y));
}

#pragma mark - Map Delegate -

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
     if ([annotation isKindOfClass:[LocationAnnotation class]]) {
     
         MKAnnotationView *annotationView = (MKAnnotationView *)
             [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
         if (annotationView == nil) {
             annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
             annotationView.enabled = YES;
             annotationView.canShowCallout = NO;
             NSString *pinName;
             if (IS_IPAD) {
                 pinName=@"pin_icon~ipad";
             }
             else
                 pinName=@"pin_icon";
             
             annotationView.image = [UIImage imageNamed:pinName];
         }
         else {
             annotationView.annotation = annotation;
         }
         return annotationView;
     }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if (IS_IPAD) {
    
        NSString *_ID = [((LocationAnnotation *)view.annotation) getID];
        [self.splitViewController setPreferredDisplayMode:UISplitViewControllerDisplayModePrimaryOverlay];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SelectPlotNotification" object:_ID];
    }
    else{
    
        NSLog(@"annotation id %@",[((LocationAnnotation *)view.annotation) getID]);
        //Add custome callout.
        // Start up our view controller from a Storyboard
        CustomCalloutViewController* controller = (CustomCalloutViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"CustomCalloutID"];
        
        // Adjust this property to change the size of the popover + content
        controller.preferredContentSize = CGSizeMake(270, 220);
        
//        if(!popover) {
            popover = [[WYPopoverController alloc] initWithContentViewController:controller];
            
            // Using WYPopoverController's iOS 6 theme
            popover.theme = [WYPopoverTheme themeForIOS7];
//        }
        
        //POP POP
        [popover presentPopoverFromRect:view.bounds inView:view permittedArrowDirections:WYPopoverArrowDirectionDown animated:TRUE  options:WYPopoverAnimationOptionFadeWithScale];
        
        DealersNetworkModel *model = networkDealers[[((LocationAnnotation *)view.annotation) locationNum]];
        [controller.name setText:[model getName]];
        [controller.address setText:[model getAddress]];
        [controller.telephone setText:[model phone]];
        
        
        [controller.serviceImage setHidden:YES];
        [controller.roomImage    setHidden:YES];
        [controller.partsImage   setHidden:YES];
        
        if (model.isServiceCenter)
            [controller.serviceImage setHidden:NO];
        else
            [controller.serviceImage setHidden:YES];
        
        if (model.isShowRoom)
            [controller.roomImage    setHidden:NO];
        else
            [controller.roomImage    setHidden:YES];
        
        if (model.isSpareParts)
            [controller.partsImage   setHidden:NO];
        else
            [controller.partsImage   setHidden:YES];
        
        
        controller.navigate.tag = [((LocationAnnotation *)view.annotation) locationNum];
        [controller.navigate addTarget:self action:@selector(openiOSMap:) forControlEvents:UIControlEventTouchUpInside];

        
    }
    
}

-(void)openiOSMap:(id)sender{
    
    UIButton *btn = (UIButton*)sender;
    NSUInteger section =  btn.tag;
    // Open iOs Map App.
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake([[networkDealers[section] latitude] doubleValue], [[networkDealers[section] longitude] doubleValue]);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:[networkDealers[section] getAddress]];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
    
    
}

/*
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    NSInteger zoomLevel = [self zoomLevelForMapRect:mapView.visibleMapRect
                            withMapViewSizeInPixels:self.view.frame.size];
    NSLog(@"%ld", (long)zoomLevel);
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation {
    return YES;
}

#pragma mark - Map Helper Function -

- (NSUInteger)zoomLevelForMapRect:(MKMapRect)mRect withMapViewSizeInPixels:(CGSize)viewSizeInPixels {
    
    NSUInteger zoomLevel = MAXIMUM_ZOOM;
    MKZoomScale zoomScale = mRect.size.width / viewSizeInPixels.width;
    double zoomExponent = log2(zoomScale);
    zoomLevel = (NSUInteger)(MAXIMUM_ZOOM - ceil(zoomExponent));
    return zoomLevel;
}
*/
- (void)setSelectedLocation:(DealersNetworkModel *)selectedLocation {
//    CLLocationCoordinate2D networkLocation = CLLocationCoordinate2DMake([selectedLocation.latitude doubleValue],
//                                                                        [selectedLocation.longitude doubleValue]);
//    MKCoordinateRegion viewregion = MKCoordinateRegionMakeWithDistance(networkLocation, 0.5 * METERS_PER_MILE,
//                                                                       0.5 * METERS_PER_MILE);
//    [self.mapView setRegion:viewregion animated:YES];
}

#pragma mark - Popover -

- (IBAction)showFilterPopover:(id)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    if (popover.isPopoverVisible) {
        [popover dismissPopoverAnimated:YES completion:^{
        }];
    }
    
/*
    FilterViewController *filterViewController = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"FilterID"];
    UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:filterViewController];
    popController.popoverContentSize = CGSizeMake(320.0f, 248.0f);
    self.popController = popController;
    [self.popController presentPopoverFromBarButtonItem:sender
                               permittedArrowDirections:UIPopoverArrowDirectionUp
                                               animated:YES];
 */
    
    NSMutableArray *categoriesNames=[[NSMutableArray alloc] init];
    NSMutableArray *categoriesIDs=[[NSMutableArray alloc] init];
    NSMutableArray *preSelected=[[NSMutableArray alloc] init];
    NSArray *servicesList;
    
    NSArray *serviceListar =  @[@{@"كل الخدمات"    : @""},
                                @{@"قطع الغيار و الخدمات"  : @"parts_icon~ipad"},
                                @{@"الخدمات"         : @"servicing_icon~ipad"},
                                @{@"المناطق"        : @"show_room_icon~ipad"}];
    
    NSArray *serviceListen = @[@{@"All Services"    : @""},
                               @{@"Parts & Services"  : @"parts_icon~ipad"},
                               @{@"Servicing"         : @"servicing_icon~ipad"},
                               @{@"Show Rooms"        : @"show_room_icon~ipad"}];
    
    
    
    if ([[Utilities getLanguage] isEqualToString:@"EN"]){
        servicesList = serviceListen;
    }
    
    else{
        servicesList = serviceListar;
    }
 

    for (int i=0; i<4; i++) {
        [categoriesNames addObject:[[KNSelectorItem alloc] initWithDisplayValue:[servicesList[i] allKeys][0]   selectValue:[servicesList[i] allKeys][0] imageUrl:nil]];
        [categoriesIDs addObject:[servicesList[i] allKeys][0]];
        
    }
    
    
    preSelected=[categoriesNames copy];
    
    /*
    NSMutableArray *savedSelectedServices=[[NSUserDefaults standardUserDefaults] objectForKey:@"Services"];
    NSLog(@"savedSelectedServices %@",savedSelectedServices);
    
    if (savedSelectedServices.count>0) {
        preSelected = categoriesNames;

        
        BOOL itemFound = false;
        
        for (int i=0; i<preSelected.count; i++) {
            for (int j=0; j<savedSelectedServices.count; j++) {
                KNSelectorItem *item = [preSelected objectAtIndex:i];
                if ([item.displayValue isEqualToString:[savedSelectedServices objectAtIndex:j]]) {
                    itemFound=YES;
                }
                if (itemFound==NO) {
                    [preSelected removeObjectAtIndex:i];
                }
            }
        }
    }
    NSLog(@"preselected %@",preSelected);
    */
    
    KNMultiItemSelector *itemSelector = [[KNMultiItemSelector alloc]initWithItems:categoriesNames _itemsIDs:categoriesIDs preselectedItems:preSelected title:@"Select Tages" placeholderText:nil delegate:nil];
    itemSelector.dismissOption = YES;

    if (IS_IPAD) {
        FilterViewController *filterViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"FilterID"];
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:filterViewController];
        popController.popoverContentSize = CGSizeMake(320.0f, 248.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        // Adjust this property to change the size of the popover + content
        itemSelector.preferredContentSize = CGSizeMake(310, 280);
        
        //        if(!popover) {
        popover = [[WYPopoverController alloc] initWithContentViewController:itemSelector];
        
        // Using WYPopoverController's iOS 6 theme
        popover.theme = [WYPopoverTheme themeForIOS7];
        //        }
        
        //POP POP
        [popover presentPopoverAsDialogAnimated:YES
                                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
 
//    [self presentModalHelper:itemSelector];
    
}
-(void)presentModalHelper:(UIViewController*)controller {
    UINavigationController * uinav = [[UINavigationController alloc] initWithRootViewController:controller];
    uinav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    uinav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:uinav animated:YES];
}
- (IBAction)showLanguagePopover:(id)sender{
    
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {
        
        
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}


- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    if (popover.isPopoverVisible) {
        [popover dismissPopoverAnimated:YES completion:^{
        }];
    }
    [self viewWillAppear:YES];
}

@end