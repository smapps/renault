//
//  CustomCalloutViewController.h
//  Renault
//
//  Created by Sameh Mabrouk on 4/13/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCalloutViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *address;

@property (weak, nonatomic) IBOutlet UILabel *telephone;
@property (weak, nonatomic) IBOutlet UIImageView *serviceImage;
@property (weak, nonatomic) IBOutlet UIImageView *roomImage;
@property (weak, nonatomic) IBOutlet UIImageView *partsImage;
@property (weak, nonatomic) IBOutlet UIView *selectionIndicatorView;

@property (weak, nonatomic) IBOutlet UILabel *contactTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *telephoneLTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *servicesTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *navigate;
@end
