//
//  ContactUsViewController.h
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController <UIActionSheetDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cellPhoneTextField;
@property (weak, nonatomic) IBOutlet UITextView  *commentsTextView;
@property (weak, nonatomic) IBOutlet UIButton    *requestType;

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (nonatomic, strong) UIPopoverController *popController;

- (IBAction)requestTypeTapped:(UIButton *)sender;
- (IBAction)submitContactInformationTapped:(UIButton *)sender;
-(IBAction)openRenaultFB:(id)sender;
-(IBAction)openRenaultInstagram:(id)sender;

@end