//
//  SpecificationsViewController.m
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "SpecificationsViewController.h"
#import "SpecificationsModel.h"
#import "SpecificationCell.h"
#import "OptionsCategoryModel.h"
#import "Utilities.h"

@interface SpecificationsViewController ()

@end

@implementation SpecificationsViewController {
    NSArray *specs;
    NSArray *optionsSpecs;
    NSInteger selectedIndex;
    NSString *selectedSpec;
    BOOL isExtended;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = 0;
    isExtended = NO;
    selectedSpec = [self.carModel.editions[selectedIndex] getName];
    specs = [NSArray arrayWithArray:[self getSelectedSpecWithIndex:selectedIndex]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSArray *)getSelectedSpecWithIndex:(NSInteger)index {
    optionsSpecs = [self.carModel.editions[selectedIndex] optionsCategories];
    optionsSpecs = [self.carModel.editions[selectedIndex] getSpecificationsFromCategory:optionsSpecs];
    return optionsSpecs;
}

#pragma mark - Tableview Datasource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return optionsSpecs.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (isExtended)
            return self.carModel.editions.count;
        else
            return 1;
    }
    else {
        return [[optionsSpecs[section - 1] catOptions] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        if (isExtended)
            [cell.textLabel setText:[self.carModel.editions[indexPath.row] getName]];
        else
            [cell.textLabel setText:selectedSpec];
        return cell;
    }
    else {
        SpecificationCell *specCell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"
                                                                    forIndexPath:indexPath];
        
        NSLog(@"Name : %@", [[[optionsSpecs[indexPath.section - 1] catOptions]
                       objectAtIndex:indexPath.row] getName]);
        NSLog(@"Description : %@", [[[optionsSpecs[indexPath.section - 1] catOptions]
                       objectAtIndex:indexPath.row] getDescription]);
        
        [specCell.nameLabel setText:[[[optionsSpecs[indexPath.section - 1] catOptions]
                                      objectAtIndex:indexPath.row] getName]];
        
        [specCell.descriptionLabel setText:[[[optionsSpecs[indexPath.section - 1] catOptions]
                                             objectAtIndex:indexPath.row] getDescription]];
        
        return specCell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if ([[Utilities getLanguage] isEqualToString:@"EN"])
            return @"Select version";
        else
            return @"اختر النوع";
    }
    else
        return [optionsSpecs[section-1] getName];
}

#pragma mark - Tableview Datasource -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSMutableArray *indexes = [NSMutableArray array];
        for (int index = 0; index < self.carModel.editions.count; index++) {
            if (index != selectedIndex) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [indexes addObject:indexPath];
            }
        }
        
        if (!isExtended) {
            isExtended = YES;
            [tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
        }
        else {
            isExtended = NO;
            [tableView deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            selectedIndex = indexPath.row;
            selectedSpec = [self.carModel.editions[selectedIndex] getName];
            specs = [NSArray arrayWithArray:[self getSelectedSpecWithIndex:selectedIndex]];
            [NSTimer scheduledTimerWithTimeInterval:0.7
                                             target:self
                                           selector:@selector(refreshTable)
                                           userInfo:nil
                                            repeats:NO];
        }
    }
}
-(void)refreshTable{
    
    [self.tableView reloadData];
    
}

@end