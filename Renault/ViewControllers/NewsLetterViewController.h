//
//  NewsLetterViewController.h
//  Renault
//
//  Created by Sameh Mabrouk on 1/2/16.
//  Copyright © 2016 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsLetterViewController : UIViewController
@property(nonatomic, strong) IBOutlet UIImageView *newsLetterImageView;
@property (nonatomic, strong) UIPopoverController *popController;

- (IBAction)changeLanguage:(UIBarButtonItem *)sender;

@end
