//
//  CarListViewController.h
//  Renault
//
//  Created by Manch on 1/26/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarListViewController : UITableViewController

- (IBAction)changeLanguage:(UIBarButtonItem *)sender;

@property (nonatomic, strong) UIPopoverController *popController;

@end