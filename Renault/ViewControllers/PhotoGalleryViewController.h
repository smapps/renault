//
//  PhotoGalleryViewController.h
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSImageViewerViewController.h"

@interface PhotoGalleryViewController : UICollectionViewController

@property (nonatomic, strong) NSArray *photoGallery;
@property (nonatomic, strong) NSMutableArray *photos;

@property(strong, nonatomic) FSImageViewerViewController *imageViewController;


- (IBAction)openGallery:(id)sender;

@end