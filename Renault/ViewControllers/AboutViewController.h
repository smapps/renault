//
//  AboutViewController.h
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *aboutHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *aboutImage;
@property (nonatomic, strong) UIPopoverController *popController;

- (IBAction)changeLanguage:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UITextView *aboutText;
@end