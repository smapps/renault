//
//  LangaugeViewController.m
//  Renault
//
//  Created by Manch on 2/9/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "LangaugeViewController.h"
#import "Constant.h"
#import "Utilities.h"

@interface LangaugeViewController ()

@end

@implementation LangaugeViewController

- (void)back {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (IS_IPHONE) {
        UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
        [self.englishButton setTitleColor:OrangeColor forState:UIControlStateNormal];
        [self.arabicButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else {
        [self.arabicButton setTitleColor:OrangeColor forState:UIControlStateNormal];
        [self.englishButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    if (IS_IPHONE) {
//        [Utilities createNewHeaderWithTitle:@"اللغة" inViewController:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)englishButtonTapped:(UIButton *)sender {
    [self.englishButton setTitleColor:OrangeColor forState:UIControlStateNormal];
    [self.arabicButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setObject:@"EN" forKey:AppLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissPopViewController" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeTabNamesNotification" object:nil];
}

- (IBAction)arabicButtonTapped:(UIButton *)sender {
    [self.arabicButton setTitleColor:OrangeColor forState:UIControlStateNormal];
    [self.englishButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] setObject:@"AR" forKey:AppLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissPopViewController" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChangeTabNamesNotification" object:nil];
}

#pragma mark - tabledatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [[UITableViewCell alloc] init];
    NSArray *titles=[[NSArray alloc] initWithObjects:@"English", @"عربي", nil];
    cell.textLabel.text=[titles objectAtIndex:indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row==0) {
        [self englishButtonTapped:nil];
    }
    else{
    
        [self arabicButtonTapped:nil];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end