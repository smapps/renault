//
//  NewsLetterViewController.m
//  Renault
//
//  Created by Sameh Mabrouk on 1/2/16.
//  Copyright © 2016 Islam Ibrahim. All rights reserved.
//

#import "NewsLetterViewController.h"
#import <UIImageView+AFNetworking.h>
#import "Constant.h"
#import "Utilities.h"
#import "LangaugeViewController.h"

@interface NewsLetterViewController ()

@end

@implementation NewsLetterViewController

-(void)closeAction{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
/*
    UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
*/

    NSString *imageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:ImageBaseURL];
    NSString *newsLetterImageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:NewsLetterImageURL];

    [self.newsLetterImageView setImageWithURL:[NSURL URLWithString:[imageBaseURL stringByAppendingString:newsLetterImageBaseURL]]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"Newsletter" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"اخبار" inViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }


    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {


        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
