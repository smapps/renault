//
//  ContactUsViewController.m
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ContactUsViewController.h"
#import "WSDispatcher+ContactUs.h"
#import "LangaugeViewController.h"
#import "Utilities.h"
#import "APIClient.h"
#import "SVProgressHUD.h"
#import "Constant.h"

@interface ContactUsViewController ()

@end

@implementation ContactUsViewController {
    NSString *selectedType;
    NSArray *typeList;
}

-(void)closeAction{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"Contact Us" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"اتصل بنا" inViewController:self];
    [self changeViewControllerLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)changeViewControllerLanguage {
    
    if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
        [self.emailTextField setPlaceholder:@"*Email"];
        [self.nameTextField setPlaceholder:@"*Name"];
        [self.cellPhoneTextField setPlaceholder:@"*Cell Phone"];
        [self.commentsTextView setText:@"Comments"];
        [self.requestType setTitle:@"Request Type" forState:UIControlStateNormal];
        [self.submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    }
    else {
        [self.emailTextField setPlaceholder:@"*البريد الالكتروني"];
        [self.nameTextField setPlaceholder:@"*الاسم"];
        [self.cellPhoneTextField setPlaceholder:@"*رقم التلبفون"];
        [self.commentsTextView setText:@"تعليقات"];
        [self.requestType setTitle:@"النوع" forState:UIControlStateNormal];
        [self.submitButton setTitle:@"ارسل" forState:UIControlStateNormal];
    }
}

- (IBAction)requestTypeTapped:(UIButton *)sender {
    
    typeList = @[@"Feedback", @"Complaint", @"Test Drive", @"Inquiry"];
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Request Type"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Feedback", @"Complaint", @"Test Drive", @"Inquiry", nil];
    [actionSheet showFromRect:[(UIButton *)sender frame] inView:self.view animated:YES];
}

- (IBAction)submitContactInformationTapped:(UIButton *)sender {
    if ([self.emailTextField.text isEqualToString:@""] ||
        [self.nameTextField.text isEqualToString:@""] ||
        [self.cellPhoneTextField.text isEqualToString:@""] || !selectedType || [self.commentsTextView.text isEqualToString:@""] ) {
        
        [[[UIAlertView alloc] initWithTitle:@"Data Missing" message:@"There are some missed data" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil] show];
    }
    else {
        
        [SVProgressHUD showWithStatus:@"Sending..." maskType:SVProgressHUDMaskTypeBlack];
        APIClient *apiClient =[APIClient sharedClient];
        
        NSDictionary *params = [NSDictionary dictionaryWithObjects:@[self.emailTextField.text, self.cellPhoneTextField.text, selectedType, self.commentsTextView.text, self.nameTextField.text]
                                                           forKeys:@[@"Email", @"MobileNo", @"ContactType", @"Inquiry", @"Name"]];
        
        [apiClient sendContactUsInfo:@"http://renaultapp.azurewebsites.net/api/ContactRequests" httpMethod:@"POST" withParams:params callback:^(BOOL success, id result) {
            
            NSString *msg;
            if ([result objectForKey:@"ID"]) {
                msg = @"your feedback sent successfully";

            }
            else{
            
                msg=@"cannot send your feedback";
            }
            
            [SVProgressHUD dismiss];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:msg message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
            self.emailTextField.text=@"";
            self.nameTextField.text=@"";
            self.commentsTextView.text=@"";
            self.cellPhoneTextField.text=@"";
        }];
        
    }
          
}
/*
- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
    popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
    self.popController = popController;
    [self.popController presentPopoverFromBarButtonItem:sender
                               permittedArrowDirections:UIPopoverArrowDirectionUp
                                               animated:YES];
}
 */
- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {
        
        
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}


-(IBAction)openRenaultFB:(id)sender{
    
    NSURL *facebookURL = [NSURL URLWithString:@"fb://profile/168004486558287"];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL]) {
        [[UIApplication sharedApplication] openURL:facebookURL];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://facebook.com/RenaultEgypt"]];
    }
}
-(IBAction)openRenaultInstagram:(id)sender{
    NSURL *instagramURL = [NSURL URLWithString:@"user?username=RENAULTEGYPT"];
    if ([[UIApplication sharedApplication] canOpenURL:instagramURL]) {
        [[UIApplication sharedApplication] openURL:instagramURL];
    }
    else{
    
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://instagram.com"]];

    }

}

- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    [self viewWillAppear:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != -1) {
        selectedType = [[NSNumber numberWithInteger:buttonIndex+1] stringValue];
        [self.requestType setTitle:typeList[buttonIndex] forState:UIControlStateNormal];
        [self.requestType setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    [textView setText:@""];
    textView.returnKeyType = UIReturnKeyDone;
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Comments";
    }
    return YES;
}

@end