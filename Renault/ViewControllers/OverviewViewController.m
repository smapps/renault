//
//  OverviewViewController.m
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "OverviewViewController.h"

#import "ImagePreview.h"
#import "SwitchCell.h"
#import "CarImageScrollerCell.h"
#import "CarDescriptionCell.h"
#import "CarColorCell.h"
#import "Utilities.h"
#import "ColorCell.h"
#import <UIImageView+AFNetworking.h>

static NSString *imagePreviewCell = @"ImagePreviewCell";
static NSString *switchViewCell   = @"SwitchPreviewCell";
static NSString *scrollerViewCell = @"ScrollPreviewCell";
static NSString *descriptionCell  = @"DescriptionPreviewCell";
static NSString *carColorCell     = @"CarColorPreviewCell";

#define ScrollImageWidth 99
#define ScrollImageHeight 70

@interface OverviewViewController ()

@end

@implementation OverviewViewController {
    BOOL isImagefetched;
    UIButton *currentButton;
    UIView *currentImageView;
    
    NSString *modelDescription;
    NSString *overviewID;
    NSInteger selectedImageIndex;
    NSInteger selectedOverviewIndex;
    NSInteger selectedCarColor;
    NSMutableArray *images;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isImagefetched = NO;
    selectedImageIndex = 0;
    selectedOverviewIndex = 0;
    selectedCarColor = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0)
        return [self fillImagePreview];
    else if (indexPath.row == 1)
        return [self fillSwitchCell];
    else if (indexPath.row == 2)
        return [self fillCarImageScrollerCell];
    else if (indexPath.row == 3)
        return [self fillCarDescription];
    else
        return [self fillCarColorFill];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return 320.0;
    else if (indexPath.row == 1)
        return 44.0;
    else if (indexPath.row == 2)
        return 70;
    else if (indexPath.row == 3) {
        /*NSString *descriptionText = [self.overviewContent[selectedOverviewIndex] getDescription];
        NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:14]};
        CGRect rect = [descriptionText boundingRectWithSize:CGSizeMake(768, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        return rect.size.height + 60;*/
        return 400;
    }
    else
        return 350.0;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Fill Static Cell -

- (ImagePreview *)fillImagePreview {
    ImagePreview *cell = [self.tableView dequeueReusableCellWithIdentifier:imagePreviewCell];
    if (!cell)
        cell = [[ImagePreview alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:imagePreviewCell];
    
    for (int index = 0; index < self.galleryContent.count; index++) {
        if ([[NSNumber numberWithInt:[[self.galleryContent[index] modelOverviewID] intValue]] isEqualToNumber:[NSNumber numberWithInt:[[self.overviewContent[selectedOverviewIndex] ID] intValue]]]) {
            [cell.carImagePreview setImageWithURL:[NSURL URLWithString:[self.galleryContent[index] Image]]];
            break;
        }
    }
    
    return cell;
}

- (SwitchCell *)fillSwitchCell {
    SwitchCell *cell = [self.tableView dequeueReusableCellWithIdentifier:switchViewCell];
    if (!cell)
        cell = [[SwitchCell alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:switchViewCell];
    
    CGFloat width = self.view.frame.size.width / self.overviewContent.count;
    for (int index = 0; index < self.overviewContent.count; index++) {
        UIButton *button = [Utilities createButtonWithTitle:[self.overviewContent[index] getName]
                                                        Tag:index Width:width offset:width*index];
        if (index == selectedOverviewIndex) {
            [button setSelected:YES];
            currentButton = button;
        }
        [button addTarget:self action:@selector(SwitchTapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:button];
    }
    for (int index = 0; index < self.overviewContent.count; index++) {
        if (index > 0 && (self.overviewContent.count - index >= 1))
            [cell addSubview:[Utilities seperationInOffset:width * index Color:[UIColor whiteColor]
                                                    Height:34.0 xOffset:5.0]];
    }
    return cell;
}

- (CarImageScrollerCell *)fillCarImageScrollerCell {
    CarImageScrollerCell *cell = [self.tableView dequeueReusableCellWithIdentifier:scrollerViewCell];
    if (!cell)
        cell = [[CarImageScrollerCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:scrollerViewCell];
    images = [NSMutableArray array];
    
    for (int index = 0; index < self.galleryContent.count; index++) {
        
        if ([[NSNumber numberWithInt:[[self.galleryContent[index] modelOverviewID] intValue]] isEqualToNumber:[NSNumber numberWithInt:[[self.overviewContent[selectedOverviewIndex] ID] intValue]]]) {
            [images addObject:self.galleryContent[index]];
        }
    }
    
    for (UIImageView *item in [cell.carImageScroller subviews]) {
        [item removeFromSuperview];
    }
    
    [cell.carImageScroller setContentSize:CGSizeMake(images.count * ScrollImageWidth, ScrollImageHeight)];
    
    for (int index = 0; index < images.count; index++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(index * ScrollImageWidth,
                                                                               0, ScrollImageWidth, ScrollImageHeight)];
        [imageView setImageWithURL:[NSURL URLWithString:[images[index] Image]]];
        [imageView setTag:index];
        [imageView setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        [tap setNumberOfTapsRequired:1];
        [tap setNumberOfTouchesRequired:1];
        [tap setCancelsTouchesInView:NO];
        [imageView addGestureRecognizer:tap];
        [cell.carImageScroller addSubview:imageView];
    }
    for (int index = 0; index < images.count; index++) {
        if (index > 0 && (images.count - index >= 1))
            [cell.carImageScroller addSubview:[Utilities seperationInOffset:ScrollImageWidth * index
                                                           Color:[UIColor blackColor] Height:ScrollImageHeight xOffset:0]];
    }
    
    currentImageView = [Utilities drawOrangeLineAt:0];
    [cell.carImageScroller addSubview:currentImageView];
    
    return cell;
}

- (CarDescriptionCell *)fillCarDescription {
    CarDescriptionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:descriptionCell];
    if (!cell)
        cell = [[CarDescriptionCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:descriptionCell];
    [cell.descriptionText setText:[self.overviewContent[selectedOverviewIndex] getDescription]];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [cell.descriptionText setTextAlignment:NSTextAlignmentLeft];
    else
        [cell.descriptionText setTextAlignment:NSTextAlignmentRight];
    return cell;
}

- (CarColorCell *)fillCarColorFill {
    CarColorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:carColorCell];
    if (!cell)
        cell = [[CarColorCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:carColorCell];
    [cell.carColorCollection setDataSource:self];
    [cell.carColorCollection setDelegate:self];
    
    [cell.carColorCollection reloadData];
    [cell.carColorPreviewImage setImageWithURL:[NSURL URLWithString:[self.colorContent[selectedCarColor] carPreview]]];
    [cell.carColorName setText:[self.colorContent[selectedCarColor] getName]];
    return cell;
}

#pragma mark - UICollection Datasource -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colorContent.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell"
                                                                           forIndexPath:indexPath];
    [cell.colorCellImage setImageWithURL:[NSURL URLWithString:[self.colorContent[indexPath.row] carColor]]];
    return cell;
}

#pragma mark - UICollection Datasource -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CarColorCell *colorCell = (CarColorCell *)[self.tableView cellForRowAtIndexPath:
                                                             [NSIndexPath indexPathForRow:4 inSection:0]];
    selectedCarColor = indexPath.row;
    [colorCell.carColorPreviewImage setImageWithURL:[NSURL URLWithString:[self.colorContent[indexPath.row] carPreview]]];
    [colorCell.carColorName setText:[self.colorContent[selectedCarColor] getName]];
}

- (void)imageTapped:(UITapGestureRecognizer *)gesture {
    ImagePreview *image = (ImagePreview *)[self.tableView cellForRowAtIndexPath:
                                           [NSIndexPath indexPathForRow:0 inSection:0]];
    NSInteger index = [[gesture view] tag];
    [image.carImagePreview setImageWithURL:[NSURL URLWithString:[images[index] Image]]];
    
    NSInteger lineOffset = index * ScrollImageWidth;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [currentImageView setTransform:CGAffineTransformMakeTranslation(lineOffset, 0)];
                     }];
}

- (void)SwitchTapped:(UIButton *)sender {
    selectedOverviewIndex = sender.tag;
    [self.tableView reloadData];
}

@end