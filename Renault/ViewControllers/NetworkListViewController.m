//
//  NetworkListViewController.m
//  Renault
//
//  Created by Manch on 2/2/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "NetworkListViewController.h"
#import "NetworkViewController.h"

#import "WSDispatcher+DealersNetwork.h"
#import "DealersNetworkModel.h"
#import "NetworkCell.h"
#import "Utilities.h"
#import "Constant.h"
#import "DealersNetworkModel.h"

@interface NetworkListViewController ()

@end

@implementation NetworkListViewController {
    NSMutableArray *locationArray;
    NSMutableArray *tempLocation;
    NSString *filterString;
    NSArray *selectedFilters;
    
    NSArray *showRoom;
    NSArray *carSales;
    NSArray *spare;
    
    NetworkCell *prevCell;
    UIView *blackDimmedView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(filterSearchResults:)
                                                 name:@"FilterSearchNotification" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getSelectedPlot:)
                                                 name:@"SelectPlotNotification" object:nil];
    
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
    else
        [SVProgressHUD showWithStatus:@"تحميل..." maskType:SVProgressHUDMaskTypeBlack];
    
    [[WSDispatcher sharedWebServiceDispatcher] getDealersNetworkWithSuccess:^(id responseObject) {
        locationArray = responseObject;
        tempLocation = responseObject;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        NSLog(@"response obj %@",responseObject);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadLocationsOnMap" object:responseObject];
        
        
        DealersNetworkModel *details = [[DealersNetworkModel alloc] init];
        NSArray *cachedCarsDetails = [details loadCashedData:@"dealersnetwork.json"];
        
        if (cachedCarsDetails.count>0) {
            
            
            NSArray *carsdetails = [details mapNetworkLocations:cachedCarsDetails];
//            NSLog(@"cached Dealers %@",carsdetails);
        }

        
    } Failure:^(NSError *error) {
        
    }];
    
    if (IS_IPHONE) {
        [self.view setHidden:YES];
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NetworkViewController *networkVC = [[[[self.splitViewController viewControllers] lastObject] viewControllers] lastObject];
    blackDimmedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, networkVC.view.frame.size.width,
                                                               networkVC.view.frame.size.height)];
    [blackDimmedView setBackgroundColor:DarkColor];
    [blackDimmedView setAlpha:0.4];
    [networkVC.view addSubview:blackDimmedView];
    
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"RENAULT Dealers" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"موزعين رينو" inViewController:self];
    [self doFilter];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NetworkViewController *networkVC = [[[[self.splitViewController viewControllers] lastObject] viewControllers] lastObject];
    [[[networkVC.view subviews] lastObject] removeFromSuperview];
    locationArray = tempLocation;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return locationArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NetworkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NetworkCell" forIndexPath:indexPath];
    if ([[Utilities getLanguage] isEqualToString:@"AR"]){
    
        cell.telephoneLTitleLabel.text = @":ت";
        cell.contactTitleLabel.text = @":اتصل بنا";
        cell.servicesTitleLabel.text = @":خدمات";
    }
    
    
    cell.navigate.tag = indexPath.section;
    [cell.navigate addTarget:self action:@selector(openiOSMap:) forControlEvents:UIControlEventTouchUpInside];
    
    DealersNetworkModel *model = locationArray[indexPath.section];
    [cell.name setText:[model getName]];
    [cell.address setText:[model getAddress]];
    [cell.telephone setText:[model phone]];
    
    [cell.serviceImage setHidden:YES];
    [cell.roomImage    setHidden:YES];
    [cell.partsImage   setHidden:YES];
    
    if (model.isServiceCenter)
        [cell.serviceImage setHidden:NO];
    else
        [cell.serviceImage setHidden:YES];
    
    if (model.isShowRoom)
        [cell.roomImage    setHidden:NO];
    else
        [cell.roomImage    setHidden:YES];
    
    if (model.isSpareParts)
        [cell.partsImage   setHidden:NO];
    else
        [cell.partsImage   setHidden:YES];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[prevCell selectionIndicatorView] setHidden:YES];
    prevCell = (NetworkCell *)[tableView cellForRowAtIndexPath:indexPath];
    [[prevCell selectionIndicatorView] setHidden:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FoucsOnPlot"
                                                        object:locationArray[indexPath.section]];
    
    NSLog(@"location Arr %@",locationArray[indexPath.section]);

    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0){
        if ([[Utilities getLanguage] isEqualToString:@"EN"])
            return [NSString stringWithFormat:@"There are %lu dealers matching your needs",
                    (unsigned long)locationArray.count];
        else
            return [NSString stringWithFormat:@"يوجد %lu موزع مطابق لاحتياجاتك",
                    (unsigned long)locationArray.count];
        
        

    }
    
    return @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 5.0;
}

-(void)openiOSMap:(id)sender{

    UIButton *btn = (UIButton*)sender;
    NSUInteger section =  btn.tag;
     // Open iOs Map App.
     Class mapItemClass = [MKMapItem class];
     if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
     {
     // Create an MKMapItem to pass to the Maps app
     CLLocationCoordinate2D coordinate =
     CLLocationCoordinate2DMake([[locationArray[section] latitude] doubleValue], [[locationArray[section] longitude] doubleValue]);
     MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
     addressDictionary:nil];
     MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
     [mapItem setName:[locationArray[section] getAddress]];
     // Pass the map item to the Maps app
     [mapItem openInMapsWithLaunchOptions:nil];
     }
    
    
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NetworkViewController *controller = (NetworkViewController *)[[self.splitViewController
                                               viewControllers] objectAtIndex:1];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        [controller setSelectedLocation:locationArray[indexPath.row]];
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

- (void)filterSearchResults:(NSNotification *)notification {
    NSLog(@"filterSearchResults");

    selectedFilters = [notification object];
    [self doFilter];
    [[prevCell selectionIndicatorView] setHidden:YES];

}

- (void)getSelectedPlot:(NSNotification *)notification {
    NSLog(@"getSelectedPlot");
    if ([notification object]) {
        NSInteger point = [self getSelectedPlotForPoint:[notification object]];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:point];
        [[prevCell selectionIndicatorView] setHidden:YES];
        prevCell = (NetworkCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        [[prevCell selectionIndicatorView] setHidden:NO];
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
        [[prevCell selectionIndicatorView] setHidden:NO];
       // [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

- (NSInteger)searchInDomain:(NSArray *)domain forID:(NSString *)ID {
    for (int index = 0; index < locationArray.count; index++) {
        if ([domain[index] ID] == ID) {
            return index;
        }
    }
    return -1;
}

- (NSInteger)getSelectedPlotForPoint:(NSString *)ID {
    return [self searchInDomain:locationArray forID:ID];
}

- (void) doFilter {
    if (selectedFilters) {
        locationArray = [NSMutableArray array];
        for (NSString *item in selectedFilters) {
            if ([item isEqualToString:@"Servicing"])
                [self filterForShowRooms];
            else if ([item isEqualToString:@"Show Rooms"])
                [self filterForCarSales];
            else if ([item isEqualToString:@"Parts & Services"])
                [self filterForSpareParts];
            else {
                locationArray = tempLocation;
            }
        }
    }
    
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DrawFilterResultsNotification"
                                                        object:locationArray];
}

- (void)filterForShowRooms {
    NSMutableArray *filter = [NSMutableArray array];
    for (DealersNetworkModel *item in tempLocation) {
        if (item.isShowRoom) {
            [filter addObject:item];
        }
    }
    [locationArray addObjectsFromArray:filter];
    showRoom = locationArray;
}

- (void)filterForSpareParts {
    NSMutableArray *filter = [NSMutableArray array];
    for (DealersNetworkModel *item in tempLocation) {
        if (item.isSpareParts)
            [filter addObject:item];
    }
    [locationArray addObjectsFromArray:filter];
    spare = locationArray;
}

- (void)filterForCarSales {
    NSMutableArray *filter = [NSMutableArray array];
    for (DealersNetworkModel *item in tempLocation) {
        if (item.isServiceCenter)
            [filter addObject:item];
    }
    [locationArray addObjectsFromArray:filter];
    carSales = locationArray;
}

-(void)openMap{

}

@end