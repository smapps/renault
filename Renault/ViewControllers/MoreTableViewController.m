//
//  MoreTableViewController.m
//  Renault
//
//  Created by Sameh Mabrouk on 1/2/16.
//  Copyright © 2016 Islam Ibrahim. All rights reserved.
//

#import "MoreTableViewController.h"
#import "Utilities.h"
#import "NewsLetterViewController.h"
#import "ContactUsViewController.h"
#import "AboutViewController.h"
#import "LangaugeViewController.h"
#import "Constant.h"

@interface MoreTableViewController ()

@end

@implementation MoreTableViewController

-(void)closeAction{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

/*
    UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
*/

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTabBarNotifications:) name:@"ChangeTabNamesNotification" object:nil];

    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"More" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"المزيد" inViewController:self];
}
- (void)changeTabBarNotifications:(NSNotification *)notifications {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MoreCell" forIndexPath:indexPath];

    // Configure the cell...
    NSString *contact;
    NSString *news;
    NSMutableArray *titles;

    if ([[Utilities getLanguage] isEqualToString:@"EN"]) {

        contact=@"Contact Us";
        news=@"About";

        titles = [[NSMutableArray alloc] initWithArray:@[contact, news]];
    }
    else{
        contact=@"تواصل معنا";
        news=@"عن رينو";

        titles = [[NSMutableArray alloc] initWithArray:@[contact, news]];

    }

//    cell.textLabel.text = titles[indexPath.row];
    UILabel *label = (UILabel *)[cell viewWithTag:100];
    label.text = titles[indexPath.row];

    UIImageView *imageView = (UIImageView *)[cell viewWithTag:200];
    if (indexPath.row == 0) {
        [imageView setImage:[UIImage imageNamed:@"coontact_us_icon"]];
    }
    else{
        [imageView setImage:[UIImage imageNamed:@"about_icon"]];
    }


    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {


//    NewsLetterViewController *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsLetterViewControllerID"];
    AboutViewController *aboutVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutViewControllerID"];

    ContactUsViewController *contactVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewControllerID"];

    if (indexPath.row == 0) {
        [self.navigationController pushViewController:contactVC animated:YES];
    }
    else
        [self.navigationController pushViewController:aboutVC animated:YES];
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }


    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {


        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation

 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
