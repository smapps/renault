//
//  FilterViewController.h
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NormalServiceCell.h"
#import "ServiceCell.h"

@interface FilterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
- (IBAction)cancelTapped:(UIButton *)sender;
- (IBAction)doneTapped:(UIButton *)sender;

@end