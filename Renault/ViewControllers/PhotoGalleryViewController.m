//
//  PhotoGalleryViewController.m
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "PhotoGalleryViewController.h"
#import "ImageCollectionCellCollectionViewCell.h"
#import "ImagePreviewViewController.h"

#import <UIImageView+AFNetworking.h>
#import "ModelGallery.h"

#import "MWPhotoBrowser.h"

#import "FSBasicImage.h"
#import "FSBasicImageSource.h"

#import "Constant.h"

@interface PhotoGalleryViewController ()<MWPhotoBrowserDelegate>

@end

@implementation PhotoGalleryViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    
    if (IS_IPAD) {
    
        flow.itemSize = CGSizeMake(256, 200);
    }
    else
        flow.itemSize = CGSizeMake(106, 90);
    
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.minimumInteritemSpacing = 0;
    flow.minimumLineSpacing = 0;
    [self.collectionView setCollectionViewLayout:flow];
    if (IS_IPHONE) {
        [self.collectionView setFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 100)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog(@"prepareForSegue");
    ImagePreviewViewController *imagePreview = [[[segue destinationViewController] viewControllers]
                                                objectAtIndex:0];
    NSInteger index = [((UIButton *)sender) tag];
    imagePreview.imageURL = [self.photoGallery[index] Image];
    imagePreview.imagesURL = self.photoGallery;
    imagePreview.selectedImageIndex = index;
    
    
}
- (IBAction)openGallery:(id)sender{
 
    NSInteger index = [((UIButton *)sender) tag];
    self.photos = [NSMutableArray array];
    for (int i=0; i<self.photoGallery.count; i++) {
        [self.photos addObject:[[FSBasicImage alloc] initWithImageURL:[NSURL URLWithString:[self.photoGallery[i] Image]] name:@""]];
    }
    FSBasicImageSource *photoSource = [[FSBasicImageSource alloc] initWithImages:self.photos];
    self.imageViewController = [[FSImageViewerViewController alloc] initWithImageSource:photoSource imageIndex:index];
    
    _imageViewController.delegate = self;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:_imageViewController];
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
    }
    else {
        [self.navigationController pushViewController:_imageViewController animated:YES];
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photoGallery.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionCellCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell.imageThumnail setImageWithURL:[NSURL URLWithString:[self.photoGallery[indexPath.row] Image]]];
    [cell.selectedElement setTag:indexPath.row];
    return cell;
}
- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {

    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

/*
#pragma mark <UICollectionViewDelegate>
 
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
