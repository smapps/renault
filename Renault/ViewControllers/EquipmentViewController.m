//
//  EquipmentViewController.m
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "EquipmentViewController.h"
#import "InformationCell.h"
#import "SpecificationsModel.h"
#import "OptionsCategoryModel.h"
#import "Utilities.h"
#import "EditionModel.h"
#import "OptionsModel.h"
#import "Constant.h"

@interface EquipmentViewController ()
@property (nonatomic, strong) NSMutableArray *cars;
@property (nonatomic, strong) NSMutableArray *info;
@end

@implementation EquipmentViewController {
    NSArray *specs;
    NSArray *optionsSpecs;
    NSInteger selectedIndex;
    NSString *selectedSpec;
    BOOL isExtended;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = 0;
    isExtended = NO;
    selectedSpec = [self.carModel.editions[selectedIndex] getName];
    specs = [NSArray arrayWithArray:[self getSelectedSpecWithIndex:selectedIndex]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSArray *)getSelectedSpecWithIndex:(NSInteger)index {
    optionsSpecs = [self.carModel.editions[selectedIndex] optionsCategories];
    optionsSpecs = [self.carModel.editions[selectedIndex] getEditionsFromCategory:optionsSpecs];
    return optionsSpecs;
}

#pragma mark - Tableview Datasource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return optionsSpecs.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (isExtended)
            return self.carModel.editions.count;
        else
            return 1;
    }
    else {
        return [[optionsSpecs[section - 1] catOptions] count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        if (isExtended)
            [cell.textLabel setText:[self.carModel.editions[indexPath.row] getName]];
        else
            [cell.textLabel setText:selectedSpec];
        return cell;
    }
    else {
        InformationCell *equipmentCell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"
                                                                      forIndexPath:indexPath];
        
        [equipmentCell.carComponentName setText:[[[optionsSpecs[indexPath.section - 1] catOptions]
                                                  objectAtIndex:indexPath.row] getName]];
        
        int status = [[[[optionsSpecs[indexPath.section - 1] catOptions]
                        objectAtIndex:indexPath.row] status] intValue];
        
        NSString *imageName = @"";
        if (status == 0){
            if (IS_IPAD) {
                imageName = @"standerd_icon~ipad";
            }
            else{
                imageName = @"standerd_icon";
            }
        }
        else if (status == 1){
            if (IS_IPAD) {
                imageName = @"optional_icon~ipad";
                
            }
            else
                imageName = @"optional_icon";
        }
        else{
            if (IS_IPAD) {
                imageName = @"unavailabe_icon~ipad";
            }
            else
                imageName = @"unavailabe_icon";
            
        }
        
        [equipmentCell.carComponentAvailabilty setImage:[UIImage imageNamed:imageName]];
        
        return equipmentCell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if ([[Utilities getLanguage] isEqualToString:@"EN"])
            return @"Select Version";
        else
            return @"اختر النوع";
    }
    else
        return [optionsSpecs[section-1] getName];
}


#pragma mark - Tableview Datasource -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSMutableArray *indexes = [NSMutableArray array];
        for (int index = 0; index < self.carModel.editions.count; index++) {
            if (index != selectedIndex) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [indexes addObject:indexPath];
            }
        }
        
        if (!isExtended) {
            NSLog(@"isExtended = YES");
            isExtended = YES;
            [tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
        }
        else {
            NSLog(@"isExtended = NO");
            isExtended = NO;
            
            [tableView deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationMiddle];
            selectedIndex = indexPath.row;
            selectedSpec = [self.carModel.editions[selectedIndex] getName];
            specs = [NSArray arrayWithArray:[self getSelectedSpecWithIndex:selectedIndex]];
            
             [NSTimer scheduledTimerWithTimeInterval:0.7
             target:self
             selector:@selector(refreshTable)
             userInfo:nil
             repeats:NO];
        }
    }
}


-(void)refreshTable{
    
    [self.tableView reloadData];
    
}


@end