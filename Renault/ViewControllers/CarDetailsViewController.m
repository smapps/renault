//
//  CarDetailsViewController.m
//  Renault
//
//  Created by Manch on 1/27/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "CarDetailsViewController.h"

#import "OverviewViewController.h"
#import "PhotoGalleryViewController.h"
#import "Utilities.h"
#import "Constant.h"
#import "OptionsModel.h"
#import "EquipmentViewController.h"
#import "LangaugeViewController.h"
#import "SpecificationsViewController.h"

@interface CarDetailsViewController ()

@end

@implementation CarDetailsViewController {
    UISegmentedControl *segmentedControl;
}

#pragma mark - UIViewController LifeCycle -

-(void)closeAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;


    
    [self initSegmentedControllAtTopOfView];
    [self initScrollView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self changeLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Initialize View -

- (OptionsModel *)getOptionModels {
    
    for (OptionsModel *item in self.optionsModel) {
        if (item.ID == self.carID) {
            return item;
        }
    }
    return nil;
}

- (void)initScrollView {
    OverviewViewController *overviewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OverviewID"];
    PhotoGalleryViewController *photoGalleryVC = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"PhotoGalleryID"];
    EquipmentViewController *equipmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EquipmentID"];
    SpecificationsViewController *specificationVC = [self.storyboard
                                                instantiateViewControllerWithIdentifier:@"SpecificationsID"];
    
    overviewVC.overviewContent   = [self.detail overviewContent];
    overviewVC.colorContent      = [self.detail carColorContent];
    overviewVC.galleryContent    = [self.detail modelGalleryContent];
    photoGalleryVC.photoGallery  = [self.detail modelGalleryContent];
    
    specificationVC.carModel     = [self getOptionModels];
    equipmentVC.carModel         = [self getOptionModels];
    
    [overviewVC.view setFrame:CGRectMake(0, self.view.frame.origin.y,
                                            overviewVC.view.frame.size.width,
                                            overviewVC.view.frame.size.height - 64)];
    
    [photoGalleryVC.view setFrame:CGRectMake(self.view.frame.size.width,
                                             self.view.frame.origin.y,
                                             photoGalleryVC.view.frame.size.width,
                                             photoGalleryVC.view.frame.size.height - 64)];
    
    [equipmentVC.view setFrame:CGRectMake(self.view.frame.size.width * 2,
                                          self.view.frame.origin.y,
                                          equipmentVC.view.frame.size.width,
                                          equipmentVC.view.frame.size.height - 64)];
    
    [specificationVC.view setFrame:CGRectMake(self.view.frame.size.width * 3,
                                              self.view.frame.origin.y,
                                              specificationVC.view.frame.size.width,
                                              specificationVC.view.frame.size.height - 64)];
    
    [self.carDetailsScroller setContentSize:CGSizeMake(self.view.bounds.size.width * 4,
                                                       self.view.bounds.size.height)];
    [self.carDetailsScroller setScrollEnabled:NO];
    
    [self.carDetailsScroller addSubview:overviewVC.view];
    [self.carDetailsScroller addSubview:photoGalleryVC.view];
    [self.carDetailsScroller addSubview:equipmentVC.view];
    [self.carDetailsScroller addSubview:specificationVC.view];
    
    [self addChildViewController:overviewVC];
    [self addChildViewController:photoGalleryVC];
    [self addChildViewController:equipmentVC];
    [self addChildViewController:specificationVC];
}

- (void)initSegmentedControllAtTopOfView {
    segmentedControl = [[UISegmentedControl alloc]
                                            initWithItems:@[@"Overview",
                                                            @"Photo Gallery",
                                                            @"Equipment",
                                                            @"Specifications"]];
    [segmentedControl addTarget:self action:@selector(careDetailsValueChanged:)
               forControlEvents:UIControlEventValueChanged];
    [segmentedControl setSelectedSegmentIndex:0];
    [segmentedControl setTintColor:OrangeColor];
    
    self.navigationItem.titleView = segmentedControl;
}

#pragma mark - UIView Event Handler -

- (void)careDetailsValueChanged:(UISegmentedControl *)sender {
    if (sender.selectedSegmentIndex == 0) {
        [self.carDetailsScroller setContentOffset:CGPointMake(0,
                                                              self.view.frame.origin.y - 64)
                                         animated:YES];
    }
    else if (sender.selectedSegmentIndex == 1) {
        [self.carDetailsScroller setContentOffset:CGPointMake(self.view.frame.size.width,
                                                              self.view.frame.origin.y - 64)
                                         animated:YES];
    }
    else if (sender.selectedSegmentIndex == 2) {
        [self.carDetailsScroller setContentOffset:CGPointMake(self.view.frame.size.width * 2,
                                                              self.view.frame.origin.y - 64)
                                         animated:YES];
    }
    else if (sender.selectedSegmentIndex == 3) {
        [self.carDetailsScroller setContentOffset:CGPointMake(self.view.frame.size.width * 3,
                                                              self.view.frame.origin.y - 64)
                                         animated:YES];
    }
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
    popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
    self.popController = popController;
    [self.popController presentPopoverFromBarButtonItem:sender
                               permittedArrowDirections:UIPopoverArrowDirectionUp
                                               animated:YES];
}

- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    for (int i=0; i<self.childViewControllers.count; i++)
        [[self.childViewControllers objectAtIndex:i] viewWillAppear:YES];
    [self changeLanguage];
}

- (void)changeLanguage {
    if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
        [segmentedControl setTitle:@"Overview" forSegmentAtIndex:0];
        [segmentedControl setTitle:@"Photo Gallery" forSegmentAtIndex:1];
        [segmentedControl setTitle:@"Equipment" forSegmentAtIndex:2];
        [segmentedControl setTitle:@"Specifications" forSegmentAtIndex:3];
    }
    else {
        [segmentedControl setTitle:@"نظرة عامة" forSegmentAtIndex:0];
        [segmentedControl setTitle:@"معرض الصور" forSegmentAtIndex:1];
        [segmentedControl setTitle:@"معدات" forSegmentAtIndex:2];
        [segmentedControl setTitle:@"مواصفات" forSegmentAtIndex:3];
    }
}

@end