//
//  ImagePreviewViewController.m
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ImagePreviewViewController.h"
#import "ModelGallery.h"
#import "Constant.h"
#import <UIImageView+AFNetworking.h>

@interface ImagePreviewViewController ()

@end

@implementation ImagePreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.scrollImage setBackgroundColor:DarkColor];
    [self.view setBackgroundColor:DarkColor];
    
    [self.scrollImage setContentSize:CGSizeMake(self.imagesURL.count * self.scrollImage.frame.size.width,
                                                self.scrollImage.frame.size.height)];
    
    for (int index = 0; index < self.imagesURL.count; index++) {
        UIImageView *image = [[UIImageView alloc]
                              initWithFrame:CGRectMake(self.scrollImage.frame.size.width * index,
                                                       self.scrollImage.frame.origin.y,
                                                       self.scrollImage.frame.size.width,
                                                       self.scrollImage.frame.size.height)];
        
        [image setImageWithURL:[NSURL URLWithString:[self.imagesURL[index] Image]]];
        [image setContentMode:UIViewContentModeScaleAspectFit];
        [self.scrollImage addSubview:image];
    }
    [self.scrollImage setContentOffset:CGPointMake(self.selectedImageIndex*self.scrollImage.frame.size.width ,
                                                   0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)getLeftImage {
    if (self.selectedImageIndex > 0)
        self.selectedImageIndex--;
    else
        self.selectedImageIndex = self.imagesURL.count-1;
    [self navigateToSelectedPage];
}

- (void)getRightImage {
    if (self.selectedImageIndex < self.imagesURL.count-1)
        self.selectedImageIndex++;
    else
        self.selectedImageIndex = 0;
    [self navigateToSelectedPage];
}

- (IBAction)dismissPreview:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)nextImageTapped:(UIButton *)sender {
    [self getRightImage];
}

- (IBAction)previousImageTapped:(UIButton *)sender {
    [self getLeftImage];
}

- (void)navigateToSelectedPage {
    [self.scrollImage setContentOffset:CGPointMake(self.selectedImageIndex * self.scrollImage.frame.size.width,
                                                   self.scrollImage.frame.origin.y)];
}

- (void)swipeLeft:(UISwipeGestureRecognizer *)sender {
    [self getLeftImage];
}

- (void)swipeRight:(UISwipeGestureRecognizer *)sender {
    [self getRightImage];
}

@end