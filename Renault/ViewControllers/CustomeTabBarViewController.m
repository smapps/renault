//
//  CustomeTabBarViewController.m
//  Renault
//
//  Created by Sameh Mabrouk on 1/2/16.
//  Copyright © 2016 Islam Ibrahim. All rights reserved.
//

#import "CustomeTabBarViewController.h"
#import "CarListViewController.h"
#import "ServicesViewController.h"

@interface CustomeTabBarViewController () <UINavigationControllerDelegate>

@end

@implementation CustomeTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSLog(@"CustomeTabBarViewController");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTabBarNotifications:) name:@"ChangeTabNamesNotification" object:nil];


//    self.moreNavigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"sam" image:[UIImage imageNamed:@"arrow.png"] tag:0];
/*
    UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Fancy Tab" image:[UIImage imageNamed:@"FancyTab"] tag:1];
    self.moreNavigationController.tabBarItem = tabBarItem; // to set the tabBarItem from outside the viewController
 */

//    self.moreNavigationController.navigationBar.tintColor = [UIColor orangeColor];
//    self.moreNavigationController.navigationBar.topItem.rightBarButtonItem = nil;


    self.moreNavigationController.delegate = self;
/*
    CarListViewController *carListController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"CarListViewControllerID"];

    ServicesViewController *servicesController = [self.storyboard
                                                instantiateViewControllerWithIdentifier:@"ServicesViewControllerID"];
 */
/*

    ServicesViewController *servicesController = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"ServicesViewControllerID"];
    UIViewController *thirdViewController = [[UIViewController alloc]init];
    thirdViewController.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:3];
    UINavigationController *thirdNavController = [[UINavigationController alloc]initWithRootViewController:thirdViewController];

    UIViewController *fourthViewController = [[UIViewController alloc]init];
    thirdViewController.tabBarItem = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:3];
    UINavigationController *fourthNavController = [[UINavigationController alloc]initWithRootViewController:fourthViewController];

*/

//    self.viewControllers = @[carListController,servicesController,carListController,carListController];
//    self.viewControllers = [[NSArray alloc] initWithObjects:thirdNavController, fourthNavController, thirdNavController, fourthNavController,servicesController, nil];
//  @[carListController,servicesController,carListController,carListController];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated
{
    navigationController.navigationBar.topItem.rightBarButtonItem = Nil;
}

- (void)changeTabBarNotifications:(NSNotification *)notifications {
    self.moreNavigationController.navigationBar.tintColor = [UIColor orangeColor];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSLog(@"prepareForSegue");
    if ([[segue identifier] isEqualToString:@"tab"]) {

        // ...
        
    } 
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
