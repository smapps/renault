//
//  CarDetailsViewController.h
//  Renault
//
//  Created by Manch on 1/27/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarsDetails.h"

@interface CarDetailsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView  *carDetailsScroller;
@property (nonatomic, strong) CarsDetails          *detail;
@property (nonatomic, strong) NSArray              *optionsModel;
@property (nonatomic, strong) UIPopoverController  *popController;
@property (nonatomic, strong) NSString *carID;

@end