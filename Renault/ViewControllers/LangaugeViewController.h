//
//  LangaugeViewController.h
//  Renault
//
//  Created by Manch on 2/9/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LangaugeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *englishButton;
@property (weak, nonatomic) IBOutlet UIButton *arabicButton;


- (IBAction)englishButtonTapped:(UIButton *)sender;
- (IBAction)arabicButtonTapped:(UIButton *)sender;

@end