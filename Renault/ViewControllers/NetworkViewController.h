//
//  NetworkViewController.h
//  Renault
//
//  Created by Manch on 2/2/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DealersNetworkModel.h"

@interface NetworkViewController : UIViewController <MKMapViewDelegate, UISplitViewControllerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) UIPopoverController *popController;
@property (nonatomic, strong) NSArray *locations;
@property (nonatomic, strong) DealersNetworkModel *selectedLocation;

- (IBAction)showFilterPopover:(id)sender;
- (IBAction)showLanguagePopover:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *filterItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *langaugeItem;

@end