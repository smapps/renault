//
//  HeaderViewController.h
//  Renault
//
//  Created by Manch on 2/9/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *headerNameLabel;

@end