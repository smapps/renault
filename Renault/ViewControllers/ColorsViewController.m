//
//  ColorsViewController.m
//  Renault
//
//  Created by Sameh Mabrouk on 4/13/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ColorsViewController.h"
#import "CarColorCell.h"
#import "Utilities.h"
#import "ColorCell.h"
#import "ModelColor.h"
#import <UIImageView+AFNetworking.h>


static NSString *carColorCell     = @"CarColorPreviewCell";

@interface ColorsViewController ()
{

    NSInteger selectedCarColor;
}
@end

@implementation ColorsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    return [self fillCarColorFill];
}

- (CarColorCell *)fillCarColorFill {
    CarColorCell *cell = [self.tableView dequeueReusableCellWithIdentifier:carColorCell];
    if (!cell)
        cell = [[CarColorCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:carColorCell];
    [cell.carColorCollection setDataSource:self];
    [cell.carColorCollection setDelegate:self];
    
    [cell.carColorCollection reloadData];
    [cell.carColorPreviewImage setImageWithURL:[NSURL URLWithString:[self.colorContent[selectedCarColor] carPreview]]];
    [cell.carColorName setText:[self.colorContent[selectedCarColor] getName]];
    return cell;
}

#pragma mark - UICollection Datasource -

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.colorContent.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell"
                                                                forIndexPath:indexPath];
    [cell.colorCellImage setImageWithURL:[NSURL URLWithString:[self.colorContent[indexPath.row] carColor]]];
    return cell;
}

#pragma mark - UICollection Datasource -

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
        NSLog(@"didSelectItemAtIndexPath");
    CarColorCell *colorCell = (CarColorCell *)[self.tableView cellForRowAtIndexPath:
                                               [NSIndexPath indexPathForRow:0 inSection:0]];
    selectedCarColor = indexPath.row;
    [colorCell.carColorPreviewImage setImageWithURL:[NSURL URLWithString:[self.colorContent[indexPath.row] carPreview]]];
    [colorCell.carColorName setText:[self.colorContent[selectedCarColor] getName]];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
