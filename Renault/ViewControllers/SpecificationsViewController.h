//
//  SpecificationsViewController.h
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditionModel.h"
#import "OptionsModel.h"

@interface SpecificationsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *editionsList;
@property (nonatomic, strong) NSArray *specList;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) OptionsModel *carModel;

@end