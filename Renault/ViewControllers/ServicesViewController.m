//
//  ServicesViewController.m
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ServicesViewController.h"
#import "ServiceModel.h"
#import "WSDispatcher+Services.h"
#import "SpecificationCell.h"
#import "LangaugeViewController.h"
#import "Constant.h"
#import "Utilities.h"

#import <SVProgressHUD.h>

#import "Reachability.h"

@interface ServicesViewController ()

@end

@implementation ServicesViewController {
    NSArray *servicesList;
    NSInteger selectedIndex;
    NSString *selectedCar;
    float total;
    BOOL isExpanded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
    total = 0;
    isExpanded = NO;
    selectedIndex = 0;
    
    ServiceModel *service = [[ServiceModel alloc] init];
    NSArray *cachedCarsServices = [service loadCashedData:@"carservices.json"];
   
    //check netwrok rechability
    if ([Reachability isNetworkRechable]) {
        
        //load cached car list data
        //refresh cached data every day.
        
        if (cachedCarsServices.count>0) {
            
            //compare cashing Date with the current date.
            int daysBetween = [self daysBetwwenCachingDateAndCurrDate];
            if (daysBetween < 1) {
                
                
                NSArray *carsServices = [service mapServicesObjects:cachedCarsServices];
                
                servicesList = carsServices;
                [self.tableView reloadData];
                selectedCar = [servicesList[selectedIndex] getCarName];
            }
            else
                [self getCarServices];
            
        }
        else{
            
            [self getCarServices];
        }
    }
    else{
    
        //load cached car services.

        if (cachedCarsServices.count>0) {
            
                NSArray *carsServices = [service mapServicesObjects:cachedCarsServices];
                
                servicesList = carsServices;
                [self.tableView reloadData];
                selectedCar = [servicesList[selectedIndex] getCarName];
            
        }
        else{
            
            //check network reachability.
            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:@"Please check your network"
                                       delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"OK", nil] show];
        }

    }
    
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (servicesList.count > 0)
        [self.tableView reloadData];
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"RENAULT Services" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"خدمات رينو" inViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCarServices{

    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
    else
        [SVProgressHUD showWithStatus:@"تحميل..." maskType:SVProgressHUDMaskTypeBlack];
    
    [[WSDispatcher sharedWebServiceDispatcher] getCarsServicesSuccess:^(id responseObject) {
        servicesList = responseObject;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
        selectedCar = [servicesList[selectedIndex] getCarName];
    } Failure:^(NSError *error) {
        
    }];

}

-(int)daysBetwwenCachingDateAndCurrDate{
    NSDate *currDate = [NSDate date];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    
    
    NSString *start = [[NSUserDefaults standardUserDefaults] objectForKey:@"SerivcesCachingDate"];
    NSString *end = [f stringFromDate:currDate];
    
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSLog(@"days between %ld", (long)[components day]);
    
    return (long)[components day];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableview Datasource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSLog(@"numberOfSectionsInTableView %lu",[[servicesList[selectedIndex] servicesType] count] + 1);
    return [[servicesList[selectedIndex] servicesType] count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (isExpanded)
            return [servicesList count];
        else
            return 1;
    }
    return [[[servicesList[selectedIndex] servicesItems] objectAtIndex:(section - 1)] count] + 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if ([[Utilities getLanguage] isEqualToString:@"EN"])
            return @"Select Car";
        else
            return @"اختار السيارة";
    }
    
    else
        return [NSString stringWithFormat:@"%@ %@",
                [[Utilities getLanguage] isEqualToString:@"EN"]? @"Services" : @"خدمات",
                [[[servicesList[selectedIndex] servicesType] objectAtIndex:section - 1] getServiceType]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        if (isExpanded)
            [cell.textLabel setText:[servicesList[indexPath.row] getCarName]];
        else
            [cell.textLabel setText:selectedCar];
        return cell;
    }
    else {
        SpecificationCell *specCell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"
                                                                      forIndexPath:indexPath];
        
        if (indexPath.row == [[[servicesList[selectedIndex] servicesItems]
                               objectAtIndex:indexPath.section - 1] count]) {
            
            
            [specCell.nameLabel setText:[[Utilities getLanguage] isEqualToString:@"EN"]? @"Total" : @"المجموع"];
            [specCell.descriptionLabel setText:[NSString stringWithFormat:@"%2.f", total]];
            [specCell setBackgroundColor:OrangeColor];
            total = 0;
        }
        else {
            [specCell.nameLabel setText:[[[[servicesList[selectedIndex] servicesItems]
                                           objectAtIndex:indexPath.section - 1]
                                          objectAtIndex:indexPath.row] getName]];
            [specCell.descriptionLabel setText:[NSString stringWithFormat:@"%2.f",
                                                [[[[[servicesList[selectedIndex] servicesItems]
                                                  objectAtIndex:indexPath.section - 1]
                                                 objectAtIndex:indexPath.row] price] floatValue]]];
            
            total += [[[[[servicesList[selectedIndex] servicesItems]
                        objectAtIndex:indexPath.section - 1]
                       objectAtIndex:indexPath.row] price] floatValue];
            [specCell setBackgroundColor:[UIColor whiteColor]];
            
        }
        return specCell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    if (section == [[servicesList[selectedIndex] servicesItems] count]) {
        if ([[Utilities getLanguage] isEqualToString:@"EN"])
            return @"*Prices don't include sales tax. Slight change in prices may be applied";
        else
            return @"*الأسعار لا تشمل ضريبة المبيعات. تغيير طفيف في الأسعار يجوز تطبيقه";
    }
    return @"";
}

#pragma mark - UITableview Delegate -

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSMutableArray *indexes = [NSMutableArray array];
        for (int index = 0; index < servicesList.count; index++) {
            if (index != selectedIndex) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                [indexes addObject:indexPath];
            }
        }
        
        if (!isExpanded) {
            isExpanded = YES;
            [self.tableView insertRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES];
        }
        else {
            isExpanded = NO;
            [tableView deleteRowsAtIndexPaths:indexes withRowAnimation:UITableViewRowAnimationAutomatic];
            selectedIndex = indexPath.row;
            selectedCar = [servicesList[selectedIndex] getCarName];
//            [tableView reloadData];
            [NSTimer scheduledTimerWithTimeInterval:0.7
                                             target:self
                                           selector:@selector(refreshTable)
                                           userInfo:nil
                                            repeats:NO];
        }
        total = 0;
    }
}

-(void)refreshTable{
    
    [self.tableView reloadData];
    
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {
        
        
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}


- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    [self viewWillAppear:YES];
}

@end