//
//  CarListViewController.m
//  Renault
//
//  Created by Manch on 1/26/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "Constant.h"
#import "CarListCell.h"
#import "CarsDetails.h"
#import "OptionsModel.h"
#import "WSDispatcher+Setting.h"
#import "CarListViewController.h"
#import "WSDispatcher+CarDetails.h"
#import "CarDetailsViewController.h"
#import "LangaugeViewController.h"
#import "HeaderViewController.h"
#import "OptionsCategoryModel.h"
#import "OverviewViewController.h"
#import "Utilities.h"

#import <UIImageView+AFNetworking.h>
#import <SVProgressHUD.h>

#import "Reachability.h"

#import "Constant.h"

#import "XHTwitterPaggingViewer.h"
#import "PhotoGalleryViewController.h"
#import "ColorsViewController.h"
#import "EquipmentViewController.h"
#import "SpecificationsViewController.h"

@interface CarListViewController ()
@property (nonatomic, strong) NSArray *carList;
@property (nonatomic, strong) NSArray *optionCategory;
@property (nonatomic, strong) XHTwitterPaggingViewer *twitterPaggingViewer;
@end

@implementation CarListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
       
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
    
    
    CarsDetails *details = [[CarsDetails alloc] init];
    OptionsModel *options = [[OptionsModel alloc] init];

    //check netwrok rechability
    if ([Reachability isNetworkRechable]) {
        NSLog(@"network reachable");
        
        //load cached car list data
        //refresh cached data every day.
        NSArray *cachedCarsDetails = [details loadCashedData:@"carlist.json"];
        NSArray *cachedModelOptions = [details loadCashedData:@"modelOptions.json"];
        
        if (cachedCarsDetails.count>0 && cachedModelOptions.count>0) {
            
            //compare cashing Date with the current date.
            int daysBetween = [self daysBetwwenCachingDateAndCurrDate];
            if (daysBetween < 1) {
            
                
                NSArray *carsdetails = [details mapCarsDetails:cachedCarsDetails];
                NSArray *optionsCategoryModels = [options mapCarModels:cachedModelOptions];
                
                self.carList = carsdetails;
                self.optionCategory = optionsCategoryModels;
                [self.tableView reloadData];
            }
            else
                [self getData];
            
        }
        else{
        
            [self getData];
        }

        
    }
    else{
    
         NSLog(@"network is not reachable");
        //load cached car list data
        NSArray *cachedCarsDetails = [details loadCashedData:@"carlist.json"];
        NSArray *cachedModelOptions = [details loadCashedData:@"modelOptions.json"];
        
        if (cachedCarsDetails.count>0 && cachedModelOptions.count>0) {

                NSArray *carsdetails = [details mapCarsDetails:cachedCarsDetails];
                NSArray *optionsCategoryModels = [options mapCarModels:cachedModelOptions];
                
                self.carList = carsdetails;
                self.optionCategory = optionsCategoryModels;
                [self.tableView reloadData];
            }
        else{
        
            //check network reachability.
            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:@"Please check your network"
                                       delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"OK", nil] show];
        }
    }
    
    
    
    
  }

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];

    
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [Utilities createNewHeaderWithTitle:@"RENAULT Cars" inViewController:self];
    else
        [Utilities createNewHeaderWithTitle:@"سيارات رينو" inViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int)daysBetwwenCachingDateAndCurrDate{
    NSDate *currDate = [NSDate date];
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    

    NSString *start = [[NSUserDefaults standardUserDefaults] objectForKey:@"cachingDate"];
    NSString *end = [f stringFromDate:currDate];
    
//    NSString *start = @"2010-09-01";
//    NSString *end = @"2010-09-03";
    

    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    NSLog(@"days between %ld", (long)[components day]);

    return (long)[components day];
}
#pragma mark - Get Data
-(void)getData{
    NSLog(@"getData");



    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        [SVProgressHUD showWithStatus:@"Loading..." maskType:SVProgressHUDMaskTypeBlack];
    else
        [SVProgressHUD showWithStatus:@"تحميل..." maskType:SVProgressHUDMaskTypeBlack];
    
    

    // Get Settings Meta data
    [[WSDispatcher sharedWebServiceDispatcher] getSettingsInformationWithSuccess:^(id responseObject) {
        [[NSUserDefaults standardUserDefaults] setValue:responseObject[@"ImagesBaseURL"] forKey:ImageBaseURL];

        // Save newsletter image url
        [[NSUserDefaults standardUserDefaults] setValue:responseObject[@"NewsLetterURL"] forKey:NewsLetterImageURL];
        NSLog(@"Response Obj %@",responseObject);
        
        // Get info about cars
        [[WSDispatcher sharedWebServiceDispatcher] getAllCarsWithInformationsWithSuccess:^(id responseObject) {
            self.carList = responseObject;
            NSLog(@"Response Obj2 %@",responseObject);

            // Get cars editions
            [[WSDispatcher sharedWebServiceDispatcher] getModelOptionsWithSuccess:^(id responseObject) {
                self.optionCategory = responseObject;
                [self.tableView reloadData];
                
                [SVProgressHUD dismiss];
            } failure:^(NSError *error) {
                NSLog(@"Error details %@",error.description);
                
                [[[UIAlertView alloc] initWithTitle:@"Error"
                                            message:@"Error while loading"
                                           delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"OK", nil] show];
                [SVProgressHUD dismiss];
            }];
            
        } failure:^(NSError *error) {
            NSLog(@"Cars Error details %@",error.description);

            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:@"Error while loading"
                                       delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:@"OK", nil] show];
            [SVProgressHUD dismiss];
        }];
        

        
    } failure:^(NSError *error) {
        
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"Error while loading"
                                   delegate:self
                          cancelButtonTitle:nil
                          otherButtonTitles:@"OK", nil] show];
        [SVProgressHUD dismiss];
    }];
    
    
    
    
   
}
#pragma mark - caching actions
-(void)cachCarList:(id)resposeObject{

    [[NSUserDefaults standardUserDefaults] setObject:resposeObject forKey:@"CashedCarList"];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.carList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CarListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"carIdent" forIndexPath:indexPath];
    [cell.carNameLabel setText:[self.carList[indexPath.row] getName]];
    [cell.carImage setImageWithURL:[NSURL URLWithString:[self.carList[indexPath.row] mainImage]]];
    [cell.carImage setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 210;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (IS_IPHONE) {
        
        _twitterPaggingViewer = [[XHTwitterPaggingViewer alloc] init];
        
        NSMutableArray *viewControllers = [[NSMutableArray alloc] initWithCapacity:7];
        NSArray *titles;
        
        if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
            titles = @[@"Overview",
                                @"Colors",
                                @"Photo Gallery",
                                @"Equipment",
                                @"Specifications"];
        }
        else {
            titles = @[@"نظرة عامة",
                                @"ألوان",
                                @"معرض الصور",
                                @"معدات",
                                @"مواصفات"];
        }

        
        [titles enumerateObjectsUsingBlock:^(NSString *title, NSUInteger idx, BOOL *stop) {
            
            if (idx==0) {
                OverviewViewController *overviewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OverviewID"];
                overviewVC.title=title;
                overviewVC.overviewContent   = [self.carList[indexPath.row] overviewContent];
                overviewVC.colorContent      = [self.carList[indexPath.row] carColorContent];
                overviewVC.galleryContent    = [self.carList[indexPath.row] modelGalleryContent];
                
                [viewControllers addObject:overviewVC];
            }
            if (idx==1) {
                
                ColorsViewController *colorsVC = [self.storyboard
                                                              instantiateViewControllerWithIdentifier:@"ColorsID"];
                colorsVC.title=title;
                colorsVC.colorContent  = [self.carList[indexPath.row] carColorContent];
                [viewControllers addObject:colorsVC];
            }
            if (idx==2) {
                PhotoGalleryViewController *photoGalleryVC = [self.storyboard
                                                              instantiateViewControllerWithIdentifier:@"PhotoGalleryID"];
                photoGalleryVC.title=title;
                photoGalleryVC.photoGallery=[self.carList[indexPath.row]modelGalleryContent];
                [viewControllers addObject:photoGalleryVC];
            }
            if (idx==3) {
                EquipmentViewController *equipmentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EquipmentID"];
                equipmentVC.title=title;
                equipmentVC.carModel = [self getOptionModels:(int)indexPath.row];
                [viewControllers addObject:equipmentVC];

            }
            if (idx==4) {
                SpecificationsViewController *specificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SpecificationsID"];
                specificationVC.title=title;
                specificationVC.carModel = [self getOptionModels:(int)indexPath.row];
                [viewControllers addObject:specificationVC];
                
            }
        }];
        
        
        _twitterPaggingViewer.viewControllers = viewControllers;
        
        _twitterPaggingViewer.didChangedPageCompleted = ^(NSInteger cuurentPage, NSString *title) {
            NSLog(@"cuurentPage : %ld on title : %@", (long)cuurentPage, title);
        };

        [self.navigationController pushViewController:_twitterPaggingViewer animated:YES];
    }
}
- (OptionsModel *)getOptionModels:(int)rowIndex {
    
    for (OptionsModel *item in self.optionCategory) {
        if (item.ID == [self.carList[rowIndex] ID]) {
            return item;
        }
    }
    return nil;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if (IS_IPAD) {
        CarDetailsViewController *detailVC = [segue destinationViewController];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        detailVC.detail = self.carList[indexPath.row];
        detailVC.optionsModel = self.optionCategory;
        detailVC.carID = [self.carList[indexPath.row] ID];
    }
    
}

- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                  instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {
    
        
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
//        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}

- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    [self viewWillAppear:YES];
}

@end