//
//  FilterViewController.m
//  Renault
//
//  Created by Manch on 2/3/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "FilterViewController.h"
#import "Utilities.h"

@interface FilterViewController ()

@end

@implementation FilterViewController {
    NSArray *servicesList;
    NSMutableArray *servicesSelected;
    NSString *filterString;
    NSString *servicesTitle;
    NSMutableArray *filters;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    filterString = ![[[NSUserDefaults standardUserDefaults] objectForKey:@"FilterKey"] isEqualToString:@""] ?[[NSUserDefaults standardUserDefaults] objectForKey:@"FilterKey"] : @"All Services";
    
//    NSArray *allservices = [[NSUserDefaults standardUserDefaults] objectForKey:@"ServicesSelected"];
//    NSLog(@"selectedServices %@",allservices);

    
    NSArray *serviceListar =  @[@{@"كل الخدمات"    : @""},
                                @{@"قطع الغيار و الخدمات"  : @"parts_icon~ipad"},
                                @{@"الخدمات"         : @"servicing_icon~ipad"},
                                @{@"المناطق"        : @"show_room_icon~ipad"}];
    
    NSArray *serviceListen = @[@{@"All Services"    : @""},
                               @{@"Parts & Services"  : @"parts_icon~ipad"},
                               @{@"Servicing"         : @"servicing_icon~ipad"},
                               @{@"Show Rooms"        : @"show_room_icon~ipad"}];
    
    if ([[Utilities getLanguage] isEqualToString:@"EN"]){
        servicesList = serviceListen;
        servicesTitle=@"All Services";
        [self.cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
        [self.doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    }
    
    else{
        servicesList = serviceListar;
        servicesTitle=@"كل الخدمات";
        [self.cancelBtn setTitle:@"إلغاء" forState:UIControlStateNormal];
        [self.doneBtn setTitle:@"تأكيد" forState:UIControlStateNormal];
    }
    filters = [[NSUserDefaults standardUserDefaults] objectForKey:@"filters"];
    NSLog(@"filters..didload%@",filters);

    /*
    for (int i=1; i<3; i++) {
        filterString=[servicesList[i] allKeys][0];
        NSLog(@"filter str %@",filterString);
        [self.tableView reloadData];
        break;
    }
     */

    servicesSelected = [NSMutableArray array];
    /*
    [NSTimer scheduledTimerWithTimeInterval:2.0
                                     target:self
                                   selector:@selector(refreshTable)
                                   userInfo:nil
                                    repeats:NO];
     */
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshTable{
    NSLog(@"refreshTable");
    for (int i=1; i<3; i++) {
        filterString=[servicesList[i] allKeys][0];
        NSLog(@"filter str %@",filterString);
        [self.tableView reloadData];
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"cellforrow");
    NSLog(@"filter string %@",filterString);
    
    if (indexPath.row == 0) {
        NormalServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NormalServiceCell" forIndexPath:indexPath];
        [cell.serviceName setText:servicesTitle];
        
        if ([filterString isEqualToString:servicesTitle])
            [cell.checkImage setHidden:NO];
        else
            [cell.checkImage setHidden:YES];
        return cell;
    }
    else {
        ServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ServiceCell" forIndexPath:indexPath];
        [cell.serviceName setText:[servicesList[indexPath.row] allKeys][0]];
        [cell.serviceImage setImage:[UIImage imageNamed:servicesList[indexPath.row][[servicesList[indexPath.row] allKeys][0]]]];
        if ([filterString isEqualToString:servicesTitle]) {
            [cell.checkImage setHidden:YES];
            servicesSelected = [NSMutableArray array];
            [servicesSelected addObject:filterString];
        }
        else if ([filterString isEqualToString:[servicesList[indexPath.row] allKeys][0]]) {
            if ([self isAllServicesExist]) {
                servicesSelected = [NSMutableArray array];
            }
            if ([self isFilterKeyExist:filterString]) {
                [cell.checkImage setHidden:YES];
                [servicesSelected removeObject:filterString];
            }
            else {
                NSLog(@"ssss");
                [cell.checkImage setHidden:NO];
                [servicesSelected addObject:filterString];
            }
        }
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    filterString = [servicesList[indexPath.row] allKeys][0];
//    NSLog(@"filter string %@",filterString);
    [[NSUserDefaults standardUserDefaults] setObject:filterString forKey:@"FilterKey"];
    [[NSUserDefaults standardUserDefaults] setObject:servicesSelected forKey:@"ServicesSelected"];
    
    if (!filters) {
        filters=[[NSMutableArray alloc] init];
    }
    if ([filters containsObject:filterString]==NO) {
        [filters addObject:filterString];
    }
    NSLog(@"filters %@",filters);
    [[NSUserDefaults standardUserDefaults] setObject:filters forKey:@"filters"];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.tableView reloadData];
}

- (BOOL)isAllServicesExist {
    if (servicesSelected.count > 0 && [servicesSelected[0] isEqualToString:@"All Services"])
        return YES;
    return NO;
}

- (BOOL)isFilterKeyExist:(NSString *)key {
    for (NSString *filterKey in servicesSelected) {
        if ([filterKey isEqualToString:key]) {
            return YES;
        }
    }
    return NO;
}

- (IBAction)cancelTapped:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DismissPopViewController" object:nil];
}

- (IBAction)doneTapped:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FilterSearchNotification" object:servicesSelected];
}

@end