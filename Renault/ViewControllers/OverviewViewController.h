//
//  OverviewViewController.h
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Overview.h"
#import "ModelColor.h"
#import "ModelGallery.h"

@interface OverviewViewController : UITableViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imagePreview;
@property (weak, nonatomic) IBOutlet UIButton *exteriorBtn;
@property (weak, nonatomic) IBOutlet UIButton *interiorBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *carImagesPreview;
@property (weak, nonatomic) IBOutlet UITextView *carDescription;
@property (weak, nonatomic) IBOutlet UIImageView *carColorImage;
@property (weak, nonatomic) IBOutlet UICollectionView *carSelectionColor;

@property (nonatomic, strong) NSArray *overviewContent;
@property (nonatomic, strong) NSArray *colorContent;
@property (nonatomic, strong) NSArray *galleryContent;

@end