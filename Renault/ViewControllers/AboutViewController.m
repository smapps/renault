//
//  AboutViewController.m
//  Renault
//
//  Created by Manch on 1/28/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "AboutViewController.h"
#import "Constant.h"
#import "LangaugeViewController.h"
#import "Utilities.h"

@interface AboutViewController ()

@end

@implementation AboutViewController {
    UISegmentedControl *segmentedController;
}

-(void)closeAction{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *buttonImage = [UIImage imageNamed:@"prev_btn.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];

    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;

    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutRenault" ofType:@"txt"];
    NSError *error;
    NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
    [self.aboutText setText:fileContents];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissPopViewController:)
                                                 name:@"DismissPopViewController"
                                               object:nil];
    
    segmentedController = [[UISegmentedControl alloc] initWithItems:@[@"Renault", @"EIM"]];
    if (IS_IPAD) {
        [segmentedController setWidth:100 forSegmentAtIndex:0];
        [segmentedController setWidth:100 forSegmentAtIndex:1];
    }
    
    
    [segmentedController addTarget:self action:@selector(aboutSegmentValueChanged:)
                  forControlEvents:UIControlEventValueChanged];
    [segmentedController setSelectedSegmentIndex:0];
    [segmentedController setTintColor:OrangeColor];
    
    [self.navigationItem setTitleView:segmentedController];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
        [segmentedController setTitle:@"Renault" forSegmentAtIndex:0];
        [segmentedController setTitle:@"EIM" forSegmentAtIndex:1];
        [self.aboutText setTextAlignment:NSTextAlignmentLeft];
    }
    else {
        [segmentedController setTitle:@"رينو" forSegmentAtIndex:0];
        [segmentedController setTitle:@"EIM" forSegmentAtIndex:1];
        [self.aboutText setTextAlignment:NSTextAlignmentRight];
    }
    [self changeLanguage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)changeLanguage {
    if (segmentedController.selectedSegmentIndex == 0) {
        if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
            NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutRenault" ofType:@"txt"];
            NSError *error;
            NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
            [self.aboutText setText:fileContents];
        }
        else {
            NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutRenaultAR" ofType:@"txt"];
            NSError *error;
            NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
            [self.aboutText setText:fileContents];
        }
    }
    else {
        if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
            NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutEIM" ofType:@"txt"];
            NSError *error;
            NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
            [self.aboutText setText:fileContents];
        }
        else {
            NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutEIMAR" ofType:@"txt"];
            NSError *error;
            NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
            [self.aboutText setText:fileContents];
        }
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)aboutSegmentValueChanged:(UISegmentedControl *)sender {
    NSString *aboutLogo;
    NSString *aboutEIMLogo;
    if (IS_IPAD) {
        aboutLogo=@"renault_logo~ipad";
        aboutEIMLogo=@"about_EIM_logo~ipad";
    }
    else{
        aboutLogo = @"renault_logo";
        aboutEIMLogo=@"about_EIM_logo";
    } 
    if (sender.selectedSegmentIndex == 0) {
      
     
        
        
        [self.aboutImage setImage:[UIImage imageNamed:aboutLogo]];
        
        NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutRenault" ofType:@"txt"];
        NSError *error;
        NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        [self.aboutText setText:fileContents];
    }
    else {
        [self.aboutImage setImage:[UIImage imageNamed:aboutEIMLogo]];
        [self.aboutHeaderView setBackgroundColor:[UIColor whiteColor]];
        
        NSString *filepath = [[NSBundle mainBundle] pathForResource:@"AboutEIM" ofType:@"txt"];
        NSError *error;
        NSString *fileContents = [NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:&error];
        [self.aboutText setText:fileContents];
    }
}
/*
- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
    popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
    self.popController = popController;
    [self.popController presentPopoverFromBarButtonItem:sender
                               permittedArrowDirections:UIPopoverArrowDirectionUp
                                               animated:YES];
}
*/
- (IBAction)changeLanguage:(UIBarButtonItem *)sender {
    if (self.popController.popoverVisible) {
        [self.popController dismissPopoverAnimated:YES];
        return;
    }
    
    
    LangaugeViewController *langaugeViewController = [self.storyboard
                                                      instantiateViewControllerWithIdentifier:@"LangaugeID"];
    if (IS_IPAD) {
        
        
        UIPopoverController *popController = [[UIPopoverController alloc] initWithContentViewController:langaugeViewController];
        popController.popoverContentSize = CGSizeMake(200.0f, 70.0f);
        self.popController = popController;
        [self.popController presentPopoverFromBarButtonItem:sender
                                   permittedArrowDirections:UIPopoverArrowDirectionUp
                                                   animated:YES];
    }
    else{
        //        [Utilities createNewHeaderWithTitle:@"Language" inViewController:langaugeViewController];
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:langaugeViewController] animated:YES completion:nil];
    }
    
}

- (void)dismissPopViewController:(NSNotification *)notification {
    [self.popController dismissPopoverAnimated:YES];
    [self viewWillAppear:YES];
}

@end