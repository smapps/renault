//
//  Utilities.m
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "Utilities.h"
#import "Constant.h"
#import "HeaderViewController.h"

@implementation Utilities

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

+ (UIButton *)createButtonWithTitle:(NSString *)title Tag:(NSInteger)tag Width:(CGFloat)width
                             offset:(CGFloat)offset {
    
    UIButton *theButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [theButton setFrame:CGRectMake(offset, 0, width, 44)];
    [theButton setTag:tag];
    [theButton setTitle:title forState:UIControlStateNormal];
    [theButton setLineBreakMode:NSLineBreakByTruncatingTail];
    [theButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [theButton setTitleColor:OrangeColor forState:UIControlStateSelected];
    [theButton setBackgroundColor:DarkColor];
    
    return theButton;
}

+ (UIView *)seperationInOffset:(CGFloat)offset Color:(UIColor *)color Height:(CGFloat)height
                       xOffset:(CGFloat)xOffset {
    UIView *seperattion = [[UIView alloc] initWithFrame:CGRectMake(offset, xOffset, 1, height)];
    [seperattion setBackgroundColor:color];
    return seperattion;
}

+ (UIView *)drawOrangeLineAt:(CGFloat)xOffset {
    UIView *horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(xOffset, 68, 99, 2)];
    [horizontalLine setBackgroundColor:OrangeColor];
    return horizontalLine;
}

+ (NSString *)getLanguage {
    return [[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage];
}

+ (UIView *)createNewHeaderWithTitle:(NSString *)title inViewController:(UIViewController *)viewController {
    HeaderViewController *headerVC = [viewController.storyboard instantiateViewControllerWithIdentifier:@"headerID"];
    [headerVC.view setFrame:CGRectMake(0, 0, 200, 34)];
    [headerVC.headerNameLabel setText:title];
    [headerVC.headerNameLabel setBackgroundColor:[UIColor clearColor]];
    [headerVC.headerNameLabel setTextAlignment:NSTextAlignmentCenter];
    [viewController.navigationItem setTitleView:headerVC.view];
    return headerVC.view;
}

@end