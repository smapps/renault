//
//  Utilities.h
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utilities : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size;
+ (UIButton *)createButtonWithTitle:(NSString *)title Tag:(NSInteger)tag Width:(CGFloat)width
                             offset:(CGFloat)offset;
+ (UIView *)seperationInOffset:(CGFloat)offset Color:(UIColor *)color Height:(CGFloat)height
                       xOffset:(CGFloat)xOffset;
+ (UIView *)drawOrangeLineAt:(CGFloat)xOffset;
+ (NSString *)getLanguage;
+ (UIView *)createNewHeaderWithTitle:(NSString *)title inViewController:(UIViewController *)viewController;

@end