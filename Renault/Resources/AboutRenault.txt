1898-1918
Louis Renault and his brothers founded their company in 1898. They quickly made a name for themselves in motor racing notching up a string of wins with their small cars. The Renault factories adopted mass production techniques in 1905 and Taylorism in 1913. During the first world war, the company turned out trucks, stretchers, ambulances, shells and even the famous FT17 tanks that made a decisive contribution to the final victory.


1919-1945
 Renault modernized its premises, starting up the first production line at Billancourt in 1929. The firm tried to keep the lid on costs in order to weather the economic crisis. But social conditions deteriorated. Louis considered the second world war and conflict with Germany as a mistake, and he gave in to the demands of the German forces. As a result, Renault was nationalized in 1945, becoming the Régie Nationale des Usines Renault (RNUR).


1945-1975
Renault, now a national corporation, modernized its plants, as well as building and buying new production sites. An attempt to conquer the US market failed but Renault nevertheless continued its international expansion. Success came with the 4CV, the first "small car for everybody", followed by the Renault 4 and Renault 5….The company then launched an upmarket model, the Renault 16, the first "voiture à vivre" (literally "car for living"). At the same time, the firm continued to achieve impressive results in rally racing.


1975-1992
The company continued to grow up to the early 1980's. The renewal of the range gathered pace with the launch of two upmarket models: Renault 25 and Espace. The brand made its mark in motorsports and entered Formula 1. But the company was losing money heavily. By initiating a drastic cost-cutting policy and refocusing on its core skills, Renault was back in the black in 1987.


1992-2005
Renault considered a merger with Volvo, but the project was dropped in 1993. The privatisation of the company in July 1996 marked a milestone in its history. Taking advantage of its newfound freedom, Renault took a stake in Nissan in 1999. The company continued to innovate and renew its range with vehicles including Mégane and Laguna. Success in Formula 1 raised the Renault brand's profile. The Renault-Nissan Alliance consolidated its structure and continued to develop new synergies. With the acquisition of Samsung Motors and Dacia, Renault accelerated its international expansion. The launch of Logan was a key part of the strategy to win emerging markets.


Since 2005
Renault considered a merger with Volvo, but the project was dropped in 1993. The privatisation of the company in July 1996 marked a milestone in its history. Taking advantage of its newfound freedom, Renault took a stake in Nissan in 1999. The company continued to innovate and renew its range with vehicles including Mégane and Laguna. Success in Formula 1 raised the Renault brand's profile. The Renault-Nissan Alliance consolidated its structure and continued to develop new synergies. With the acquisition of Samsung Motors and Dacia, Renault accelerated its international expansion. The launch of Logan was a key part of the strategy to win emerging markets.
