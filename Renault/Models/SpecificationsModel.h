//
//  SpecificationsModel.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecificationsModel : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *optionID;
@property (nonatomic, strong) NSString *editionID;
@property (nonatomic, strong) NSString *descriptionEN;
@property (nonatomic, strong) NSString *descriptionAR;

- (NSString *)getName;
- (NSString *)getDescription;
- (NSArray  *)getSpecificationsObjects:(NSArray *)specifications distinctObject:(NSArray *)distinctCategories;

@end