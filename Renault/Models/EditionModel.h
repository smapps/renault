//
//  EditionModel.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EditionModel : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *modelID;
@property (nonatomic, strong) NSArray *distinctCategories;
@property (nonatomic, strong) NSArray *specifications;

- (NSArray *)mapEditionsObjects:(NSArray *)editions;
- (NSString *)getName;

@end