//
//  EditionModel.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "EditionModel.h"
#import "CategoryModel.h"
#import "SpecificationsModel.h"
#import "Constant.h"

@implementation EditionModel

- (NSArray *)mapEditionsObjects:(NSArray *)editions {
    NSMutableArray *mappedEditions = [NSMutableArray array];
    
    for (NSDictionary *edition in editions) {
        EditionModel *model = [[EditionModel alloc] init];
        model.ID = edition[@"ID"];
        model.modelID = edition[@"ModelID"];
        model.nameAR = edition[@"NameAr"];
        model.nameEN = edition[@"NameEn"];
        
        CategoryModel *category = [[CategoryModel alloc] init];
        SpecificationsModel *Specifications = [[SpecificationsModel alloc] init];
        
        model.distinctCategories = [category getDistinctcategories:edition[@"Specifications"]];
        model.specifications = [Specifications getSpecificationsObjects:edition[@"Specifications"]
                                                            distinctObject:model.distinctCategories];
        
        [mappedEditions addObject:model];
    }
    return mappedEditions;
}

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

@end