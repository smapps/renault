//
//  Overview.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "Overview.h"
#import "Constant.h"

@implementation Overview

- (NSArray *)mapOverviewOject:(NSArray *)overviewResponse {
    NSMutableArray *mappedOverview = [NSMutableArray array];
    for (NSDictionary *overviewItem in overviewResponse) {
        Overview *mappedObject = [Overview new];
        mappedObject.ID = overviewItem[@"ID"];
        mappedObject.nameEN = overviewItem[@"NameEn"];
        mappedObject.nameAR = overviewItem[@"NameAr"];
        mappedObject.DescriptionEN = overviewItem[@"DescriptionEn"];
        mappedObject.DescriptionAR = overviewItem[@"DescriptionAr"];
        
        [mappedOverview addObject:mappedObject];
    }
    return mappedOverview;
}

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

- (NSString *)getDescription {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.DescriptionEN;
    else
        return self.DescriptionAR;
}

@end