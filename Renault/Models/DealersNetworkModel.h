//
//  DealersNetworkModel.h
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DealersNetworkModel : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *addressEN;
@property (nonatomic, strong) NSString *addressAR;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (assign) BOOL isShowRoom;
@property (assign) BOOL isSpareParts;
@property (assign) BOOL isServiceCenter;

- (NSString *)getName;
- (NSString *)getAddress;
- (NSArray *)mapNetworkLocations:(NSArray *)locationResponse;

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName;
-(NSMutableArray*)loadCashedData:(NSString*)fileName;

@end