//
//  DealersNetworkModel.m
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "DealersNetworkModel.h"
#import "Constant.h"

@implementation DealersNetworkModel

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

- (NSString *)getAddress {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.addressEN;
    return self.addressAR;
}

- (NSArray *)mapNetworkLocations:(NSArray *)locationResponse {
    NSMutableArray *network = [NSMutableArray array];
    
    for (NSDictionary *dealer in locationResponse) {
        DealersNetworkModel *model = [[DealersNetworkModel alloc] init];
        model.ID = dealer[@"ID"];
        model.nameAR = dealer[@"NameAr"];
        model.nameEN = dealer[@"NameEn"];
        model.addressEN = dealer[@"AddressEn"];
        model.addressAR = dealer[@"AddressAr"];
        model.phone = dealer[@"Phone"];
        model.latitude = dealer[@"Latitude"];
        model.longitude = dealer[@"Longitude"];
        
        model.isShowRoom = [dealer[@"IsShowRoom"] boolValue];
        model.isSpareParts = [dealer[@"IsSpareParts"] boolValue];
        model.isServiceCenter = [dealer[@"IsServiceCenter"] boolValue];
        
        [network addObject:model];
    }
    
    return network;
}

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName{
    
    NSError *e;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resposeObject options:0 error:&e];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    @try {
        [jsonData writeToFile:localFilePath atomically:YES];
        //save caching date.
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:currDate];
        NSLog(@"%@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:@"DealersNetworkCachingDate"];
        NSLog(@"data written...");
        
    }
    @catch (NSException *exception) {
        NSLog(@"EXception %@",exception.description);
    }
    @finally {
        
    }
    
}

-(NSMutableArray*)loadCashedData:(NSString*)fileName{
    NSMutableArray *jsonList;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    NSData *myData = [NSData dataWithContentsOfFile:localFilePath];
    if (myData) {
        NSError *e;
        jsonList = [NSJSONSerialization JSONObjectWithData:myData options:NSJSONReadingMutableContainers error:&e];
    }
    return jsonList;
    
}


@end