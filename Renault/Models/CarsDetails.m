//
//  CarsDetails.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "CarsDetails.h"
#import "Constant.h"

@implementation CarsDetails

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.ID forKey:@"ID"];
    [aCoder encodeObject:self.nameAR forKey:@"nameAR"];
    [aCoder encodeObject:self.nameEN forKey:@"nameEN"];
    [aCoder encodeObject:self.mainImage forKey:@"mainImage"];
    [aCoder encodeObject:self.overviewContent forKey:@"overviewContent"];
    [aCoder encodeObject:self.carColorContent forKey:@"carColorContent"];
    [aCoder encodeObject:self.modelGalleryContent forKey:@"modelGalleryContent"];
    [aCoder encodeObject:self.editionsContent forKey:@"editionsContent"];

}

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

- (NSArray *)mapCarsDetails:(id)response {
    NSMutableArray *carDetails = [NSMutableArray array];
    NSString *imageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:ImageBaseURL];
    
    for (NSDictionary *item in response) {
        CarsDetails *details = [[CarsDetails alloc] init];
        
        Overview *overviews = [[Overview alloc] init];
        ModelColor *modelColor = [[ModelColor alloc] init];
        ModelGallery *modelGallery = [[ModelGallery alloc] init];
        EditionModel *editions = [[EditionModel alloc] init];
        
        details.overviewContent = [overviews mapOverviewOject:item[@"Overviews"]];
        details.carColorContent = [modelColor mapModelColors:item[@"ModelsColors"]];
        details.modelGalleryContent = [modelGallery mapModelGallery:item[@"ModelsGallery"]];
        details.editionsContent = [editions mapEditionsObjects:item[@"Editions"]];
        
        details.ID = item[@"ID"];
        details.nameAR = item[@"NameAr"];
        details.nameEN = item[@"NameEn"];
        details.mainImage = [NSString stringWithFormat:@"%@%@.png", imageBaseURL, item[@"MainImg"]];
        
        [carDetails addObject:details];
    }
    return carDetails;
}

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName{

    NSError *e;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resposeObject options:0 error:&e];
//    NSMutableArray *jsonList = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
//    NSLog(@"jsonList: %@", jsonList);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    @try {
        [jsonData writeToFile:localFilePath atomically:YES];
        //save caching date.
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:currDate];
        NSLog(@"%@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:@"cachingDate"];
        NSLog(@"data written...");
        
    }
    @catch (NSException *exception) {
        NSLog(@"EXception %@",exception.description);
    }
    @finally {
        
    }

}

-(NSMutableArray*)loadCashedData:(NSString*)fileName{
    NSMutableArray *jsonList;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];

    NSData *myData = [NSData dataWithContentsOfFile:localFilePath];
    if (myData) {
        NSError *e;
        jsonList = [NSJSONSerialization JSONObjectWithData:myData options:NSJSONReadingMutableContainers error:&e];
//        NSLog(@"Saved jsonList: %@", jsonList);
    }
    return jsonList;

}
@end