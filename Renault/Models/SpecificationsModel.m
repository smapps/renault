//
//  SpecificationsModel.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "SpecificationsModel.h"
#import "Constant.h"
#import "CategoryModel.h"

@implementation SpecificationsModel

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

- (NSArray *)mapSpecificationsModel:(NSArray *)specifications {
    NSMutableArray *mappedSpecs =[NSMutableArray array];
    
    for (NSDictionary *specs in specifications) {
        SpecificationsModel *specItem = [[SpecificationsModel alloc] init];
        specItem.ID = specs[@"ID"];
        specItem.optionID = specs[@"Option"][@"ID"];
        
        specItem.nameEN = !([specs[@"Option"][@"NameEn"] isKindOfClass:[NSNull class]]) ?
                            specs[@"Option"][@"NameEn"] : @"";
        specItem.nameAR = !([specs[@"Option"][@"NameAr"] isKindOfClass:[NSNull class]]) ?
                            specs[@"Option"][@"NameAr"] : @"";

        specItem.descriptionEN = !([specs[@"DescriptionEn"] isKindOfClass:[NSNull class]]) ?
                            specs[@"DescriptionEn"] : @"";
        specItem.descriptionAR = !([specs[@"DescriptionEn"] isKindOfClass:[NSNull class]]) ?
                            specs[@"DescriptionAr"] : @"";
        [mappedSpecs addObject:specItem];
    }
    
    return mappedSpecs;
}

- (NSString *)getDescription {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.descriptionEN;
    else
        return self.descriptionAR;
}

- (NSArray  *)getSpecificationsObjects:(NSArray *)specifications distinctObject:(NSArray *)distinctCategories {
    NSMutableArray *specObject = [NSMutableArray array];
    for (CategoryModel *category in distinctCategories) {
        
        NSArray *mappedSpecs = [self mapSpecificationsModel:[self getRelatedOptionsForCategoryObject:category
                                                                                Specifications:specifications]];
        [specObject addObject:mappedSpecs];
    }
    return specObject;
}

- (NSArray *)getRelatedOptionsForCategoryObject:(CategoryModel *)category
                                 Specifications:(NSArray *)specifications {
    
    NSMutableArray *relatedOptions = [NSMutableArray array];
    
    for (NSDictionary *item in specifications) {
        if ([[item[@"Option"][@"OptionCategory"] stringValue]
             isEqualToString:[NSString stringWithFormat:@"%@", category.ID]]) {
            [relatedOptions addObject:item];
        }
    }
    
    return relatedOptions;
}

@end