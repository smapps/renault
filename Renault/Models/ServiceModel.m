//
//  ServiceModel.m
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ServiceModel.h"
#import "Constant.h"

@implementation ServiceItem

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.itemNameEN;
    return self.itemNameAR;
}

- (NSArray *)mapItems:(NSArray *)itemResposne {
    NSMutableArray *items = [NSMutableArray array];
    
    for (NSDictionary *item in itemResposne) {
        
        NSMutableArray *servicesItems = [NSMutableArray array];
        for (NSDictionary *serviceItem in item[@"ServicesItems"]) {
            
            ServiceItem *serviceModel = [ServiceItem new];
            serviceModel.price      = serviceItem[@"Price"];
            serviceModel.itemNameAR = serviceItem[@"NameAr"];
            serviceModel.itemNameEN = serviceItem[@"NameEn"];
            [servicesItems addObject:serviceModel];
        }
        
        [items addObject:servicesItems];
    }
    return items;
}

@end

@implementation ServiceType

- (NSString *)getServiceType {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.serviceTypeEN;
    return self.serviceTypeAR;
}

- (NSArray *)mapServicesItems:(NSArray *)response {
    NSMutableArray *items = [NSMutableArray array];
    for (NSDictionary *item in response) {
        ServiceType *serviceModel = [ServiceType new];
        serviceModel.serviceTypeAR = item[@"NameAr"];
        serviceModel.serviceTypeEN = item[@"NameEn"];
        [items addObject:serviceModel];
    }
    return items;
}

@end

@implementation ServiceModel

- (NSString *)getCarName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.carNameEN;
    return self.carNameAR;
}

- (NSArray *)mapServicesObjects:(NSArray *)response {
    NSMutableArray *services = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        ServiceModel *serviceModel = [ServiceModel new];
        ServiceItem *serviceItem = [ServiceItem new];
        ServiceType *serviceType = [ServiceType new];
        
        serviceModel.carNameEN = item[@"NameEn"];
        serviceModel.carNameAR = item[@"NameAr"];
        serviceModel.servicesType = [serviceType mapServicesItems:item[@"Services"]];
        serviceModel.servicesItems = [serviceItem mapItems:item[@"Services"]];
        
        [services addObject:serviceModel];
    }
    
    return services;
}

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName{
    
    NSError *e;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resposeObject options:0 error:&e];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    @try {
        [jsonData writeToFile:localFilePath atomically:YES];
        //save caching date.
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateString = [dateFormatter stringFromDate:currDate];
        NSLog(@"%@",dateString);
        [[NSUserDefaults standardUserDefaults] setObject:dateString forKey:@"SerivcesCachingDate"];
        NSLog(@"data written...");
        
    }
    @catch (NSException *exception) {
        NSLog(@"EXception %@",exception.description);
    }
    @finally {
        
    }
    
}

-(NSMutableArray*)loadCashedData:(NSString*)fileName{
    NSMutableArray *jsonList;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    NSData *myData = [NSData dataWithContentsOfFile:localFilePath];
    if (myData) {
        NSError *e;
        jsonList = [NSJSONSerialization JSONObjectWithData:myData options:NSJSONReadingMutableContainers error:&e];
    }
    return jsonList;
    
}


@end
