//
//  CategoryModel.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "CategoryModel.h"
#import "Constant.h"

@implementation CategoryModel

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

- (NSArray *)mapCategoryObject:(NSArray *)categories {
    NSMutableArray *mappedcategories = [NSMutableArray array];
    for (NSDictionary *item in categories) {
        CategoryModel *category = [[CategoryModel alloc] init];
        category.ID = item[@"Option"][@"OptionsCategorie"][@"ID"];
        category.nameEN = item[@"Option"][@"OptionsCategorie"][@"NameEn"];
        category.nameAR = item[@"Option"][@"OptionsCategorie"][@"NameAr"];
        
        [mappedcategories addObject:category];
    }
    return mappedcategories;
}

- (NSArray *)getDistinctcategories:(NSArray *)specifications {
    NSMutableArray *distinctID = [NSMutableArray array];
    for (NSDictionary *optionCategory in specifications) {
        if (!([self isExist:optionCategory[@"Option"][@"OptionsCategorie"][@"ID"] Specifications:distinctID]))
            [distinctID addObject:optionCategory];
    }
    return [self mapCategoryObject:distinctID];
}

- (BOOL)isExist:(NSString *)ID Specifications:(NSArray *)distinct {
    for (NSDictionary *catID in distinct)
        if ([[catID[@"Option"][@"OptionsCategorie"][@"ID"] stringValue]
             isEqualToString:[NSString stringWithFormat:@"%@", ID]])
            return YES;
    return NO;
}

@end