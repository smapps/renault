//
//  LocationAnnotation.h
//  Renault
//
//  Created by Manch on 2/7/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface LocationAnnotation : NSObject <MKAnnotation>

@property(nonatomic)int locationNum;

- (id)initWithID:(NSString *)ID Name:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate;
- (MKMapItem*)mapItem;
- (NSString *)getID;

@end