//
//  ModelColor.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelColor : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *carColor;
@property (nonatomic, strong) NSString *carPreview;
@property (nonatomic, strong) NSString *selectorImage;

- (NSArray *)mapModelColors:(NSArray *)modelColors;
- (NSString *)getName;

@end