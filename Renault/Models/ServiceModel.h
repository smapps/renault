//
//  ServiceModel.h
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceItem : NSObject

@property (nonatomic, strong) NSString *itemNameEN;
@property (nonatomic, strong) NSString *itemNameAR;
@property (nonatomic, strong) NSString *price;

- (NSString *)getName;
- (NSArray *)mapItems:(NSArray *)itemResposne;

@end

@interface ServiceType : NSObject

@property (nonatomic, strong) NSString *serviceTypeEN;
@property (nonatomic, strong) NSString *serviceTypeAR;

- (NSString *)getServiceType;
- (NSArray *)mapServicesItems:(NSArray *)response;

@end

@interface ServiceModel : NSObject

@property (nonatomic, strong) NSArray  *servicesType;
@property (nonatomic, strong) NSArray  *servicesItems;

@property (nonatomic, strong) NSString *carNameAR;
@property (nonatomic, strong) NSString *carNameEN;

- (NSString *)getCarName;
- (NSArray *)mapServicesObjects:(NSArray *)response;

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName;
-(NSMutableArray*)loadCashedData:(NSString*)fileName;

@end