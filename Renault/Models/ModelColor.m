//
//  ModelColor.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ModelColor.h"
#import "Constant.h"

@implementation ModelColor

- (NSArray *)mapModelColors:(NSArray *)modelColors {
    NSMutableArray *models = [NSMutableArray array];
    NSString *imageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:ImageBaseURL];
    
    for (NSDictionary *item in modelColors) {
        ModelColor *model = [[ModelColor alloc] init];
        model.ID = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        model.carColor = [NSString stringWithFormat:@"%@%@.png", imageBaseURL, item[@"ColorPreviewImg"]];
        model.carPreview = [NSString stringWithFormat:@"%@%@.png", imageBaseURL, item[@"CarPreviewImg"]];
        model.selectorImage =[NSString stringWithFormat:@"%@%@.png", imageBaseURL, item[@"SelectorImg"]];
        
        [models addObject:model];
    }
    return models;
}

- (NSString *)getName {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:AppLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    else
        return self.nameAR;
}

@end