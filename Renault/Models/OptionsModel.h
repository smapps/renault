//
//  OptionsModel.h
//  Renault
//
//  Created by Manch on 2/13/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Equipment : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *status;

- (NSArray *)getEquipment:(NSArray *)response;
- (NSString *)getName;

@end

@interface Specification : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *descriptionEN;
@property (nonatomic, strong) NSString *descriptionAR;

- (NSArray *)getSpecification:(NSArray *)response;
- (NSString *)getName;
- (NSString *)getDescription;

@end

@interface OptionsCategories : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (assign)            BOOL      isEquipment;
@property (nonatomic, strong) NSArray  *catOptions;

- (NSArray *)getOptionsCategories:(NSArray *)response;
- (NSString *)getName;

@end

@interface Editions : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSArray  *optionsCategories;

- (NSString *)getName;
- (NSArray  *)getEditionsModels            :(NSArray *)response;
- (NSArray  *)getEditionsFromCategory      :(NSArray *)categories;
- (NSArray  *)getSpecificationsFromCategory:(NSArray *)categories;

@end

@interface OptionsModel : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSArray  *editions;

- (NSArray *)mapCarModels:(NSArray *)response;
- (NSString *)getName;
-(void)cachData:(id)resposeObject fileName:(NSString*)fileName;
-(NSMutableArray*)loadCashedData:(NSString*)fileName;

@end