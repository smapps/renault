//
//  ModelGallery.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelGallery : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *Image;
@property (nonatomic, strong) NSString *modelID;
@property (nonatomic, strong) NSString *modelOverviewID;

- (NSArray *)mapModelGallery:(NSArray *)modelGalleryResponse;

@end