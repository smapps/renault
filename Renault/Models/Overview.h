//
//  Overview.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Overview : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *DescriptionEN;
@property (nonatomic, strong) NSString *DescriptionAR;

- (NSString *)getName;
- (NSString *)getDescription;
- (NSArray *)mapOverviewOject:(NSArray *)overviewResponse;

@end