//
//  OptionsCategoryModel.m
//  Renault
//
//  Created by Manch on 2/9/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "OptionsCategoryModel.h"
#import "Utilities.h"

@implementation SpecificationsOptionModel

- (SpecificationsOptionModel *)mapSpecificationsOptionsModel:(NSDictionary *)response {
    self.specsModels = [NSMutableArray array];
    
    for (NSDictionary *item in response[@"Options"]) {
        SpecificationsOptionModel *model = [SpecificationsOptionModel new];
        model.nameAR = item[@"NameAr"];
        model.nameEN = item[@"NameEn"];
        model.descriptionEN = [item[@"DescriptionEn"] isKindOfClass:[NSNull class]] ? @"" : item[@"DescriptionEn"];
        model.descriptionAR = [item[@"DescriptionAr"] isKindOfClass:[NSNull class]] ? @"" : item[@"DescriptionAr"];
        [self.specsModels addObject:model];
    }
    self.specNameAR = response[@"NameAr"];
    self.specNameEN = response[@"NameEn"];
    return self;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

- (NSString *)getDescription {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.descriptionEN;
    return self.descriptionAR;
}

- (NSString *)getSpecName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.specNameEN;
    return self.specNameAR;
}

@end

@implementation EquipmentOptionModel

- (EquipmentOptionModel *)mapEquipmentModel:(NSDictionary *)response {
    self.equipmentsModels = [NSMutableArray array];
    
    for (NSDictionary *item in response[@"Options"]) {
        EquipmentOptionModel *model = [EquipmentOptionModel new];
        model.nameAR = item[@"NameAr"];
        model.nameEN = item[@"NameEn"];
        model.status = item[@"Status"];
        [self.equipmentsModels addObject:model];
    }
    
    self.eqNameAR = response[@"NameAr"];
    self.eqNameEN = response[@"NameEn"];
    
    return self;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

- (NSString *)getEquipName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.eqNameEN;
    return self.eqNameAR;
}

@end

@implementation OptionsCategoryModel

- (NSArray *)mapOptionsModelResponse:(NSArray *)response {
    NSMutableArray *options = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        OptionsCategoryModel *model = [OptionsCategoryModel new];
        model.equipmentOptions      = [NSMutableArray array];
        model.specificationsOptions = [NSMutableArray array];
        
        model.ID     = item[@"ID"];
        model.nameAR = item[@"NameAr"];
        model.nameEN = item[@"NameEn"];
        
        for (NSDictionary *optionsItem in item[@"OptionsCategories"]) {
            
            if ([optionsItem[@"Equipment"] boolValue]) {
                EquipmentOptionModel *eqModel = [EquipmentOptionModel new];
                [model.equipmentOptions addObject:[eqModel mapEquipmentModel:optionsItem]];
            }
            
            else {
                SpecificationsOptionModel *specModel = [SpecificationsOptionModel new];
                [model.specificationsOptions addObject:[specModel mapSpecificationsOptionsModel:optionsItem]];
            }
            
        }
        [options addObject:model];
    }
    
    return options;
}

@end