//
//  ModelGallery.m
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "ModelGallery.h"
#import "Constant.h"

@implementation ModelGallery

- (NSArray *)mapModelGallery:(NSArray *)modelGalleryResponse {
    NSMutableArray *gallery = [NSMutableArray array];
    NSString *imageBaseURL = [[NSUserDefaults standardUserDefaults] objectForKey:ImageBaseURL];
    
    for (NSDictionary *item in modelGalleryResponse) {
        ModelGallery *model = [[ModelGallery alloc] init];
        model.ID = item[@"ID"];
        model.Image = [NSString stringWithFormat:@"%@%@.png", imageBaseURL, item[@"Img"]];
        model.modelID = item[@"ModelID"];
        model.modelOverviewID = [item[@"OverviewID"] isKindOfClass:[NSNull class]] ? nil : item[@"OverviewID"];
        
        [gallery addObject:model];
    }
    
    return gallery;
}

@end