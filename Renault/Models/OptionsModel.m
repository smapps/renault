//
//  OptionsModel.m
//  Renault
//
//  Created by Manch on 2/13/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "OptionsModel.h"
#import "Utilities.h"

@implementation Equipment

- (NSArray *)getEquipment:(NSArray *)response {
    NSMutableArray *equipment = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        Equipment *model = [[Equipment alloc] init];
        model.ID     = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        model.status = item[@"Status"];
        
        [equipment addObject:model];
    }
    
    return equipment;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

@end

@implementation Specification

- (NSArray *)getSpecification:(NSArray *)response {
    NSMutableArray *specification = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        Specification *model = [[Specification alloc] init];
        model.ID     = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        model.descriptionEN = [item[@"DescriptionEn"] isKindOfClass:[NSNull class]] ?
                                                                            @"" : item[@"DescriptionEn"];
        model.descriptionAR = [item[@"DescriptionAr"] isKindOfClass:[NSNull class]] ?
                                                                            @"" : item[@"DescriptionAr"];
        
        [specification addObject:model];
    }
    
    return specification;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

- (NSString *)getDescription {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.descriptionEN;
    return self.descriptionAR;
}

@end

@implementation OptionsCategories

- (NSArray *)getOptionsCategories:(NSArray *)response {
    NSMutableArray *category = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        OptionsCategories *model = [[OptionsCategories alloc] init];
        model.ID     = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        model.isEquipment = [item[@"Equipment"] boolValue];
        
        if (model.isEquipment) {
            Equipment *equipment = [[Equipment alloc] init];
            model.catOptions = [equipment getEquipment:item[@"Options"]];
        }
        else {
            Specification *equipment = [[Specification alloc] init];
            model.catOptions = [equipment getSpecification:item[@"Options"]];
        }
        
        [category addObject:model];
    }
    
    return category;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

@end

@implementation Editions

- (NSArray  *)getSpecificationsFromCategory:(NSArray *)categories {
    NSMutableArray *specs = [NSMutableArray array];
    
    for (OptionsCategories *item in categories)
        if (![item isEquipment])
            [specs addObject:item];
    
    return specs;
}

- (NSArray  *)getEditionsFromCategory:(NSArray *)categories {
    NSMutableArray *editions = [NSMutableArray array];
    
    for (OptionsCategories *item in categories)
        if ([item isEquipment])
            [editions addObject:item];
    
    return editions;
}

- (NSArray *)getEditionsModels:(NSArray *)response {
    NSMutableArray *editions = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        Editions *model = [[Editions alloc] init];
        model.ID     = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        
        OptionsCategories *optionCategory = [[OptionsCategories alloc] init];
        model.optionsCategories = [optionCategory getOptionsCategories:item[@"OptionsCategories"]];
        
        [editions addObject:model];
    }
    
    return editions;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

@end

@implementation OptionsModel

- (NSArray *)mapCarModels:(NSArray *)response {
    NSMutableArray *cars = [NSMutableArray array];
    
    for (NSDictionary *item in response) {
        OptionsModel *model = [[OptionsModel alloc] init];
        model.ID = item[@"ID"];
        model.nameEN = item[@"NameEn"];
        model.nameAR = item[@"NameAr"];
        
        Editions *editions = [[Editions alloc] init];
        model.editions = [editions getEditionsModels:item[@"Editions"]];
        
        [cars addObject:model];
    }
    
    return cars;
}

- (NSString *)getName {
    if ([[Utilities getLanguage] isEqualToString:@"EN"])
        return self.nameEN;
    return self.nameAR;
}

-(void)cachData:(id)resposeObject fileName:(NSString*)fileName{
    
    NSError *e;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:resposeObject options:0 error:&e];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    @try {
        [jsonData writeToFile:localFilePath atomically:YES];
    }
    @catch (NSException *exception) {
        NSLog(@"EXception %@",exception.description);
    }
    @finally {
        
    }
    
}

-(NSMutableArray*)loadCashedData:(NSString*)fileName{
    NSMutableArray *jsonList;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    NSString *localFilePath = [path stringByAppendingPathComponent:fileName];
    
    NSData *myData = [NSData dataWithContentsOfFile:localFilePath];
    if (myData) {
        NSError *e;
        jsonList = [NSJSONSerialization JSONObjectWithData:myData options:NSJSONReadingMutableContainers error:&e];
        //        NSLog(@"Saved jsonList: %@", jsonList);
    }
    return jsonList;
    
}

@end
