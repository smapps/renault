//
//  CarsDetails.h
//  Renault
//
//  Created by Manch on 2/6/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "Overview.h"
#import "ModelColor.h"
#import "ModelGallery.h"
#import "SpecificationsModel.h"
#import "EditionModel.h"
#import <Foundation/Foundation.h>

@interface CarsDetails : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *mainImage;

@property (nonatomic, strong) NSArray *overviewContent;
@property (nonatomic, strong) NSArray *carColorContent;
@property (nonatomic, strong) NSArray *modelGalleryContent;
@property (nonatomic, strong) NSArray *editionsContent;

- (NSArray *)mapCarsDetails:(id)response;
- (NSString *)getName;
-(void)cachData:(id)resposeObject fileName:(NSString*)fileName;
-(NSMutableArray*)loadCashedData:(NSString*)fileName;

@end