//
//  OptionsCategoryModel.h
//  Renault
//
//  Created by Manch on 2/9/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecificationsOptionModel : NSObject

@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSString *descriptionAR;
@property (nonatomic, strong) NSString *descriptionEN;

@property (nonatomic, strong) NSString *specNameEN;
@property (nonatomic, strong) NSString *specNameAR;

@property (nonatomic, strong) NSMutableArray *specsModels;

- (NSArray *)mapSpecificationsOptionsModel:(NSDictionary *)response;
- (NSString *)getName;
- (NSString *)getSpecName;
- (NSString *)getDescription;

@end

@interface EquipmentOptionModel : NSObject

@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;
@property (nonatomic, strong) NSNumber *status;

@property (nonatomic, strong) NSString *eqNameEN;
@property (nonatomic, strong) NSString *eqNameAR;

@property (nonatomic, strong) NSMutableArray *equipmentsModels;

- (NSArray *)mapEquipmentModel:(NSDictionary *)response;
- (NSString *)getName;
- (NSString *)getEquipName;

@end

@interface OptionsCategoryModel : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *nameEN;
@property (nonatomic, strong) NSString *nameAR;

@property (nonatomic, strong) NSMutableArray *equipmentOptions;
@property (nonatomic, strong) NSMutableArray *specificationsOptions;

- (NSArray *)mapOptionsModelResponse:(NSArray *)response;

@end