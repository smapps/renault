//
//  InformationCell.h
//  Renault
//
//  Created by Manch on 2/1/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *carComponentName;
@property (weak, nonatomic) IBOutlet UIImageView *carComponentAvailabilty;

@end