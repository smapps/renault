//
//  ImageCollectionCellCollectionViewCell.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageCollectionCellCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectedElement;
@property (strong, nonatomic) IBOutlet UIImageView *imageThumnail;

@end