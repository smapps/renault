//
//  CarDescriptionCell.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarDescriptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextView *descriptionText;

@end