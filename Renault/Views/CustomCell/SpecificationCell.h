//
//  SpecificationCell.h
//  Renault
//
//  Created by Manch on 2/8/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecificationCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;

@end