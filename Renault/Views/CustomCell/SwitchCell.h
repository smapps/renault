//
//  SwitchCell.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *exteriorBtn;
@property (weak, nonatomic) IBOutlet UIButton *interiorBtn;
@end
