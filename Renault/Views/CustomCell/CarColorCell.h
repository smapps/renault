//
//  CarColorCell.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarColorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *carColorCollection;
@property (weak, nonatomic) IBOutlet UIImageView *carColorPreviewImage;
@property (weak, nonatomic) IBOutlet UILabel *carColorName;

@end