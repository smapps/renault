//
//  CarImageScrollerCell.h
//  Renault
//
//  Created by Manch on 1/31/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarImageScrollerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIScrollView *carImageScroller;

@end