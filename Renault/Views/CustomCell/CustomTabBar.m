//
//  CustomTabBar.m
//  Renault
//
//  Created by Manch on 2/1/15.
//  Copyright (c) 2015 Islam Ibrahim. All rights reserved.
//

#import "CustomTabBar.h"
#import "Utilities.h"
#import "Constant.h"

@implementation CustomTabBar

- (id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"initWithCoder, Custom cell");
    if (self = [super initWithCoder:aDecoder]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTabBarNotifications:) name:@"ChangeTabNamesNotification" object:nil];
        if (![Utilities getLanguage])
        {

            [[NSUserDefaults standardUserDefaults] setValue:@"EN" forKey:AppLanguage];
        }
        [self customizeTabBar];
    }
    return self;
}

- (void)customizeTabBar {
    NSString *cars;
    NSString *services;
    NSString *network;
    NSString *about;
    NSString *contact;
    NSString *news;
    NSString *more;
/*
    if (IS_IPHONE) {
        cars=@"";
        services=@"";
        network=@"";
        about=@"";

        if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
            news=@"News";
            contact=@"Contact";

        }
        else{
            news=@"اخبار";
            contact=@"تواصل معنا";
        }

    }
    else{
*/
        if ([[Utilities getLanguage] isEqualToString:@"EN"]) {
            cars=@"Cars";
            services=@"Services";
            network=@"Network";
            about=@"About Us";
            contact=@"Contact";
            news=@"Newsletter";
            more = @"More";
        }
        else{
            cars=@"سيارات";
            services=@"خدمات";
            network=@"شبكة";
            about=@"عن رينو";
            contact=@"تواصل معنا";
            news=@"اخبار";
            more = @"المزيد";

        }
//    }
    NSLog(@"Change Lang");

    [self initializeTabBarIndex:0 withTitle:cars       basicImage:@"cars_icon_inactive"
                  selectedImage:@"cars_icon_active"];
    [self initializeTabBarIndex:1 withTitle:services   basicImage:@"services_icon_inactive"
                  selectedImage:@"services_icon_active"];
    [self initializeTabBarIndex:2 withTitle:network    basicImage:@"network_icon_inactive"
                  selectedImage:@"network_icon_active" ];
    [self initializeTabBarIndex:3 withTitle:news   basicImage:@"newsletter_icon_unselected"
                  selectedImage:@"newsletter_icon_selected"];
    [self initializeTabBarIndex:4 withTitle:more    basicImage:@"more_icon_unselected"
                  selectedImage:@"more_icon_selected"];
    

    //[self initializeTabBarIndex:5 withTitle:news    basicImage:@"newsletter_icon_unselected"
      //            selectedImage:@"newsletter_icon_selected"];
    NSLog(@"Change Lang..end");
    [self setBarTintColor:[UIColor blackColor]];

//    UITabBarItem *i = self.items[4];
//    [i setTitle:@""];

}

- (void)initializeTabBarIndex:(NSInteger)index withTitle:(NSString *)title
                   basicImage:(NSString *)image selectedImage:(NSString *)selectedImage {
    NSLog(@"items %@", self.items);
    NSLog(@"items count %lu", self.items.count);
    
    UITabBarItem *customBarItem = [self.items objectAtIndex:index];
    [customBarItem setTag:index];
    [customBarItem setTitle:title];
    UIImage *feedImage = [UIImage imageNamed:image];
    UIImage *feedSelectedImage = [UIImage imageNamed:selectedImage];
    [customBarItem setImage:[feedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [customBarItem setSelectedImage:[feedSelectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    UIColor *selectedTintColor = [UIColor colorWithRed:252.0/255.0 green:180.0/255.0 blue:22.0/255.0 alpha:1.0];
    UIColor *basicTintColor = [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0];

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName :
                                                            [UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSForegroundColorAttributeName :
                                                            selectedTintColor
                                                        } forState:UIControlStateSelected];

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSFontAttributeName :
                                                            [UIFont fontWithName:@"HelveticaNeue" size:10.0f], NSForegroundColorAttributeName :
                                                            basicTintColor
                                                        } forState:UIControlStateNormal];
}

- (void)changeTabBarNotifications:(NSNotification *)notifications {
    [self customizeTabBar];
}

@end